<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutNddClass</name>
    <message>
        <location filename="aboutndd.ui" line="20"/>
        <location filename="ui_aboutndd.h" line="70"/>
        <source>AboutNdd</source>
        <translation type="unfinished">关于Ndd</translation>
    </message>
    <message>
        <location filename="aboutndd.ui" line="39"/>
        <location filename="ui_aboutndd.h" line="71"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://gitee.com/cxasm/notepad--&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#0000ff;&quot;&gt;Click to get the latest version of notepad-- or source code&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://gitee.com/cxasm/notepad--&quot;&gt;&lt;span style=&quot; font-size:10pt; text-decoration: underline; color:#0000ff;&quot;&gt;点击获取最新版的 notepad-- 或开源代码&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="aboutndd.ui" line="49"/>
        <location filename="ui_aboutndd.h" line="72"/>
        <source>opensoure web：https://gitee.com/cxasm/notepad--</source>
        <translation type="unfinished">开源网址:https://gitee.com/cxasm/notepad--</translation>
    </message>
</context>
<context>
    <name>AlignWinClass</name>
    <message>
        <location filename="alignwin.ui" line="20"/>
        <location filename="ui_alignwin.h" line="139"/>
        <source>Pull Open</source>
        <translation type="unfinished">拉开操作</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="36"/>
        <location filename="ui_alignwin.h" line="140"/>
        <source>Pull Open Line Set</source>
        <translation type="unfinished">拉开行设置</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="42"/>
        <location filename="ui_alignwin.h" line="141"/>
        <source>Pull out the section with insufficient comparison results.</source>
        <translation type="unfinished">拉开对比结果不满意的部分行</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="51"/>
        <location filename="ui_alignwin.h" line="142"/>
        <source>Start LineNum:</source>
        <translation type="unfinished">开始行：</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="65"/>
        <location filename="ui_alignwin.h" line="143"/>
        <source>End LineNum:</source>
        <translation type="unfinished">结束行：</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="99"/>
        <location filename="ui_alignwin.h" line="144"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="alignwin.ui" line="119"/>
        <location filename="ui_alignwin.h" line="145"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>CCNotePad</name>
    <message>
        <source>File</source>
        <translation type="obsolete">文件</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">编辑</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="110"/>
        <location filename="ui_ccnotepad.h" line="1588"/>
        <source>Format Conversion</source>
        <translation type="unfinished">格式转换</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">查找</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="obsolete">视图</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="211"/>
        <location filename="ui_ccnotepad.h" line="1596"/>
        <source>Display symbols</source>
        <translation type="unfinished">显示符号</translation>
    </message>
    <message>
        <source>Encoding</source>
        <translation type="obsolete">编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="485"/>
        <location filename="ui_ccnotepad.h" line="1623"/>
        <source>Language</source>
        <translation type="unfinished">语言</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="267"/>
        <location filename="ui_ccnotepad.h" line="1602"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="282"/>
        <location filename="cceditor/ccnotepad.ui" line="874"/>
        <location filename="ui_ccnotepad.h" line="1396"/>
        <location filename="ui_ccnotepad.h" line="1603"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="295"/>
        <location filename="ui_ccnotepad.h" line="1604"/>
        <source>J</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="304"/>
        <location filename="cceditor/ccnotepad.ui" line="842"/>
        <location filename="ui_ccnotepad.h" line="1392"/>
        <location filename="ui_ccnotepad.h" line="1605"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="314"/>
        <location filename="ui_ccnotepad.h" line="1606"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="320"/>
        <location filename="ui_ccnotepad.h" line="1607"/>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="329"/>
        <location filename="ui_ccnotepad.h" line="1608"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="338"/>
        <location filename="ui_ccnotepad.h" line="1609"/>
        <source>I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="346"/>
        <location filename="ui_ccnotepad.h" line="1610"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="355"/>
        <location filename="ui_ccnotepad.h" line="1611"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="367"/>
        <location filename="ui_ccnotepad.h" line="1612"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="379"/>
        <location filename="ui_ccnotepad.h" line="1613"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="389"/>
        <location filename="ui_ccnotepad.h" line="1614"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="397"/>
        <location filename="ui_ccnotepad.h" line="1615"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="406"/>
        <location filename="ui_ccnotepad.h" line="1616"/>
        <source>F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="415"/>
        <location filename="cceditor/ccnotepad.ui" line="1103"/>
        <location filename="cceditor/ccnotepad.ui" line="1452"/>
        <location filename="ui_ccnotepad.h" line="1425"/>
        <location filename="ui_ccnotepad.h" line="1469"/>
        <location filename="ui_ccnotepad.h" line="1617"/>
        <source>D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="422"/>
        <location filename="ui_ccnotepad.h" line="1618"/>
        <source>O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="429"/>
        <location filename="ui_ccnotepad.h" line="1619"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="437"/>
        <location filename="ui_ccnotepad.h" line="1620"/>
        <source>G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="obsolete">设置</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="471"/>
        <location filename="ui_ccnotepad.h" line="1622"/>
        <source>Style</source>
        <translation type="unfinished">皮肤风格</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="540"/>
        <location filename="ui_ccnotepad.h" line="1630"/>
        <source>About</source>
        <translation type="unfinished">关于</translation>
    </message>
    <message>
        <source>Compare</source>
        <translation type="obsolete">对比</translation>
    </message>
    <message>
        <source>Recently</source>
        <translation type="obsolete">最近对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="527"/>
        <location filename="ui_ccnotepad.h" line="1628"/>
        <source>Dir ...</source>
        <translation type="unfinished">目录...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="532"/>
        <location filename="ui_ccnotepad.h" line="1629"/>
        <source>File ...</source>
        <translation type="unfinished">文件...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="573"/>
        <location filename="ui_ccnotepad.h" line="1307"/>
        <source>New</source>
        <translation type="unfinished">新建</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="584"/>
        <location filename="ui_ccnotepad.h" line="1311"/>
        <source>Open ...</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="587"/>
        <location filename="ui_ccnotepad.h" line="1313"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="592"/>
        <location filename="ui_ccnotepad.h" line="1315"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="595"/>
        <location filename="ui_ccnotepad.h" line="1317"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="600"/>
        <location filename="ui_ccnotepad.h" line="1319"/>
        <source>Save As ...</source>
        <translation type="unfinished">另存为</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="603"/>
        <location filename="ui_ccnotepad.h" line="1321"/>
        <source>Ctrl+Alt+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="608"/>
        <location filename="cceditor/ccnotepad.cpp" line="2109"/>
        <location filename="cceditor/ccnotepad.cpp" line="2448"/>
        <location filename="ui_ccnotepad.h" line="1323"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="611"/>
        <location filename="ui_ccnotepad.h" line="1325"/>
        <source>Ctrl+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="616"/>
        <location filename="ui_ccnotepad.h" line="1327"/>
        <source>Exit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="619"/>
        <location filename="ui_ccnotepad.h" line="1329"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="624"/>
        <location filename="cceditor/ccnotepad.cpp" line="2115"/>
        <location filename="ui_ccnotepad.h" line="1331"/>
        <source>Close All</source>
        <translation type="unfinished">关闭所有</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="627"/>
        <location filename="ui_ccnotepad.h" line="1333"/>
        <source>Ctrl+Shift+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="632"/>
        <location filename="cceditor/ccnotepad.cpp" line="2143"/>
        <location filename="ui_ccnotepad.h" line="1335"/>
        <source>Undo</source>
        <translation type="unfinished">撤销</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="635"/>
        <location filename="ui_ccnotepad.h" line="1337"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="640"/>
        <location filename="cceditor/ccnotepad.cpp" line="2149"/>
        <location filename="ui_ccnotepad.h" line="1339"/>
        <source>Redo</source>
        <translation type="unfinished">重做</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="643"/>
        <location filename="ui_ccnotepad.h" line="1341"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="648"/>
        <location filename="cceditor/ccnotepad.cpp" line="2123"/>
        <location filename="ui_ccnotepad.h" line="1343"/>
        <source>Cut</source>
        <translation type="unfinished">剪切</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="651"/>
        <location filename="ui_ccnotepad.h" line="1345"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="656"/>
        <location filename="ui_ccnotepad.h" line="1347"/>
        <source>Copy</source>
        <translation type="unfinished">拷贝</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="659"/>
        <location filename="ui_ccnotepad.h" line="1349"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="664"/>
        <location filename="cceditor/ccnotepad.cpp" line="2135"/>
        <location filename="ui_ccnotepad.h" line="1351"/>
        <source>Paste</source>
        <translation type="unfinished">粘贴</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="667"/>
        <location filename="ui_ccnotepad.h" line="1353"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="672"/>
        <location filename="ui_ccnotepad.h" line="1355"/>
        <source>Select All</source>
        <translation type="unfinished">全选</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="675"/>
        <location filename="ui_ccnotepad.h" line="1357"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="683"/>
        <location filename="ui_ccnotepad.h" line="1359"/>
        <source>Windows(CR+LF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="691"/>
        <location filename="ui_ccnotepad.h" line="1360"/>
        <source>Unix(LF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="699"/>
        <location filename="ui_ccnotepad.h" line="1361"/>
        <source>Mac(CR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="704"/>
        <location filename="cceditor/ccnotepad.cpp" line="2157"/>
        <location filename="ui_ccnotepad.h" line="1362"/>
        <source>Find</source>
        <translation type="unfinished">查找</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="707"/>
        <location filename="ui_ccnotepad.h" line="1364"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="712"/>
        <location filename="cceditor/ccnotepad.cpp" line="2163"/>
        <location filename="ui_ccnotepad.h" line="1366"/>
        <source>Replace</source>
        <translation type="unfinished">替换</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="720"/>
        <location filename="ui_ccnotepad.h" line="1370"/>
        <source>Go line</source>
        <translation type="unfinished">跳转</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="723"/>
        <location filename="ui_ccnotepad.h" line="1372"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="731"/>
        <location filename="ui_ccnotepad.h" line="1374"/>
        <source>Show spaces/tabs</source>
        <translation type="unfinished">显示空格</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="739"/>
        <location filename="ui_ccnotepad.h" line="1375"/>
        <source>Show end of line</source>
        <translation type="unfinished">显示行尾</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="747"/>
        <location filename="ui_ccnotepad.h" line="1376"/>
        <source>Show all</source>
        <translation type="unfinished">显示所有</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="755"/>
        <location filename="ui_ccnotepad.h" line="1377"/>
        <source>Encode in GBK</source>
        <translation type="unfinished">编码 GBK</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="763"/>
        <location filename="ui_ccnotepad.h" line="1378"/>
        <source>Encode in UTF8</source>
        <translation type="unfinished">编码 UTF8</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="771"/>
        <location filename="ui_ccnotepad.h" line="1379"/>
        <source>Encode in UTF8-BOM</source>
        <translation type="unfinished">编码 UTF8-BOM</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="779"/>
        <location filename="ui_ccnotepad.h" line="1380"/>
        <source>Encode in UCS-2 BE BOM</source>
        <translation type="unfinished">编码 UCS-2 BE BOM</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="787"/>
        <location filename="ui_ccnotepad.h" line="1381"/>
        <source>Encode in UCS-2 LE BOM</source>
        <translation type="unfinished">编码 UCS-2 LE BOM</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="792"/>
        <location filename="ui_ccnotepad.h" line="1382"/>
        <source>Convert to GBK</source>
        <translation type="unfinished">转换为 GBK 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="797"/>
        <location filename="ui_ccnotepad.h" line="1383"/>
        <source>Convert to UTF8</source>
        <translation type="unfinished">转换为 UTF8 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="802"/>
        <location filename="ui_ccnotepad.h" line="1384"/>
        <source>Convert to UTF8-BOM</source>
        <translation type="unfinished">转换为 UTF8-BOM 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="807"/>
        <location filename="ui_ccnotepad.h" line="1385"/>
        <source>Convert to UCS-2 BE BOM</source>
        <translation type="unfinished">转换为 UCS-2 BE BOM 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="812"/>
        <location filename="ui_ccnotepad.h" line="1386"/>
        <source>Convert to UCS-2 LE BOM</source>
        <translation type="unfinished">转换为 UCS-2 LE BOM 编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="817"/>
        <location filename="ui_ccnotepad.h" line="1387"/>
        <source>Batch convert</source>
        <translation type="unfinished">批量转换编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="822"/>
        <location filename="ui_ccnotepad.h" line="1388"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="827"/>
        <location filename="ui_ccnotepad.h" line="1389"/>
        <source>BugFix</source>
        <translation type="unfinished">问题反馈</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1536"/>
        <location filename="ui_ccnotepad.h" line="1481"/>
        <source>Donate</source>
        <translation type="unfinished">捐赠作者</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1547"/>
        <location filename="ui_ccnotepad.h" line="1482"/>
        <source>Default</source>
        <translation type="unfinished">默认</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1555"/>
        <location filename="ui_ccnotepad.h" line="1483"/>
        <source>LightBlue</source>
        <translation type="unfinished">亮蓝</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1563"/>
        <location filename="ui_ccnotepad.h" line="1484"/>
        <source>ThinBlue</source>
        <translation type="unfinished">淡蓝</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1571"/>
        <location filename="ui_ccnotepad.h" line="1485"/>
        <source>RiceYellow</source>
        <translation type="unfinished">纸黄</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1579"/>
        <location filename="cceditor/ccnotepad.ui" line="1904"/>
        <location filename="ui_ccnotepad.h" line="1486"/>
        <location filename="ui_ccnotepad.h" line="1566"/>
        <source>Yellow</source>
        <translation type="unfinished">黄色</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1587"/>
        <location filename="ui_ccnotepad.h" line="1487"/>
        <source>Silver</source>
        <translation type="unfinished">银色</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1595"/>
        <location filename="ui_ccnotepad.h" line="1488"/>
        <source>LavenderBlush</source>
        <translation type="unfinished">淡紫红</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1603"/>
        <location filename="ui_ccnotepad.h" line="1489"/>
        <source>MistyRose</source>
        <translation type="unfinished">浅玫瑰色</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1611"/>
        <location filename="ui_ccnotepad.h" line="1490"/>
        <source>English</source>
        <translation type="unfinished">英文</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1619"/>
        <location filename="ui_ccnotepad.h" line="1491"/>
        <source>Chinese</source>
        <translation type="unfinished">中文</translation>
    </message>
    <message>
        <source>donate</source>
        <translation type="obsolete">捐赠软件</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1508"/>
        <location filename="ui_ccnotepad.h" line="1476"/>
        <source>TXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1531"/>
        <location filename="ui_ccnotepad.h" line="1480"/>
        <source>Search Result</source>
        <translation type="unfinished">查找结果</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1513"/>
        <location filename="cceditor/ccnotepad.ui" line="1518"/>
        <location filename="ui_ccnotepad.h" line="1477"/>
        <location filename="ui_ccnotepad.h" line="1478"/>
        <source>test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1526"/>
        <location filename="ui_ccnotepad.h" line="1479"/>
        <source>go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="832"/>
        <location filename="ui_ccnotepad.h" line="1390"/>
        <source>File compare</source>
        <translation type="unfinished">文件对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="20"/>
        <location filename="ui_ccnotepad.h" line="1306"/>
        <source>notepad--</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="90"/>
        <location filename="ui_ccnotepad.h" line="1586"/>
        <source>File(&amp;F)</source>
        <translation type="unfinished">文件(&amp;F)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="106"/>
        <location filename="ui_ccnotepad.h" line="1587"/>
        <source>Edit(&amp;E)</source>
        <translation type="unfinished">编辑(&amp;E)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="118"/>
        <location filename="ui_ccnotepad.h" line="1589"/>
        <source>Blank CharOperate</source>
        <translation type="unfinished">空白字符操作</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="130"/>
        <location filename="ui_ccnotepad.h" line="1590"/>
        <source>Convert Case to</source>
        <translation type="unfinished">大小写转换</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="143"/>
        <location filename="ui_ccnotepad.h" line="1591"/>
        <source>Line Operations</source>
        <translation type="unfinished">行编辑</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="183"/>
        <location filename="ui_ccnotepad.h" line="1592"/>
        <source>Search(&amp;S)</source>
        <translation type="unfinished">查找(&amp;S)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="187"/>
        <location filename="ui_ccnotepad.h" line="1593"/>
        <source>Book Mark</source>
        <translation type="unfinished">书签</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="192"/>
        <location filename="ui_ccnotepad.h" line="1594"/>
        <source>Mark Color</source>
        <translation type="unfinished">标记颜色...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="207"/>
        <location filename="ui_ccnotepad.h" line="1595"/>
        <source>View(&amp;V)</source>
        <translation type="unfinished">视图(&amp;V)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="219"/>
        <location filename="ui_ccnotepad.h" line="1597"/>
        <source>Icon Size</source>
        <translation type="unfinished">图标大小</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="232"/>
        <location filename="ui_ccnotepad.h" line="1598"/>
        <source>Encoding(&amp;N)</source>
        <translation type="unfinished">编码(&amp;N)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="236"/>
        <location filename="ui_ccnotepad.h" line="1599"/>
        <source>Other</source>
        <translation type="unfinished">其它编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="242"/>
        <location filename="ui_ccnotepad.h" line="1600"/>
        <source>Convert to Other</source>
        <translation type="unfinished">转换为其它编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="263"/>
        <location filename="ui_ccnotepad.h" line="1601"/>
        <source>Language(&amp;L)</source>
        <translation type="unfinished">语言(&amp;L)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="467"/>
        <location filename="ui_ccnotepad.h" line="1621"/>
        <source>Set(&amp;T)</source>
        <translation type="unfinished">设置(&amp;T)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="492"/>
        <location filename="ui_ccnotepad.h" line="1624"/>
        <source>Format Language</source>
        <translation type="unfinished">格式化语言</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="507"/>
        <location filename="ui_ccnotepad.h" line="1625"/>
        <source>Feedback</source>
        <translation type="unfinished">反馈问题</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="515"/>
        <location filename="ui_ccnotepad.h" line="1626"/>
        <source>Compare(&amp;C)</source>
        <translation type="unfinished">对比(&amp;C)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="523"/>
        <location filename="ui_ccnotepad.h" line="1627"/>
        <source>Recently(&amp;R)</source>
        <translation type="unfinished">最近对比(&amp;R)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="576"/>
        <location filename="ui_ccnotepad.h" line="1309"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="715"/>
        <location filename="ui_ccnotepad.h" line="1368"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="837"/>
        <location filename="ui_ccnotepad.h" line="1391"/>
        <source>Dir compare</source>
        <translation type="unfinished">目录对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="850"/>
        <location filename="ui_ccnotepad.h" line="1393"/>
        <source>XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="858"/>
        <location filename="ui_ccnotepad.h" line="1394"/>
        <source>YAML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="866"/>
        <location filename="ui_ccnotepad.h" line="1395"/>
        <source>Php</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="882"/>
        <location filename="ui_ccnotepad.h" line="1397"/>
        <source>C++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="890"/>
        <location filename="ui_ccnotepad.h" line="1398"/>
        <source>C#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="898"/>
        <location filename="ui_ccnotepad.h" line="1399"/>
        <source>Objective C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="906"/>
        <location filename="ui_ccnotepad.h" line="1400"/>
        <source>Java</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="914"/>
        <location filename="ui_ccnotepad.h" line="1401"/>
        <source>RC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="922"/>
        <location filename="ui_ccnotepad.h" line="1402"/>
        <source>HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="930"/>
        <location filename="ui_ccnotepad.h" line="1403"/>
        <source>Makefile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="938"/>
        <location filename="ui_ccnotepad.h" line="1404"/>
        <source>Pascal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="946"/>
        <location filename="ui_ccnotepad.h" line="1405"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="954"/>
        <location filename="ui_ccnotepad.h" line="1406"/>
        <source>ini</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="962"/>
        <location filename="ui_ccnotepad.h" line="1407"/>
        <source>Nfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="970"/>
        <location filename="ui_ccnotepad.h" line="1408"/>
        <source>Asp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="978"/>
        <location filename="ui_ccnotepad.h" line="1409"/>
        <source>Sql</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="986"/>
        <location filename="ui_ccnotepad.h" line="1410"/>
        <source>Virsual Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="994"/>
        <location filename="ui_ccnotepad.h" line="1411"/>
        <source>JavaScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1002"/>
        <location filename="ui_ccnotepad.h" line="1412"/>
        <source>Css</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1010"/>
        <location filename="ui_ccnotepad.h" line="1413"/>
        <source>Perl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1018"/>
        <location filename="ui_ccnotepad.h" line="1414"/>
        <source>Python</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1026"/>
        <location filename="ui_ccnotepad.h" line="1415"/>
        <source>Lua</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1034"/>
        <location filename="ui_ccnotepad.h" line="1416"/>
        <source>Tex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1042"/>
        <location filename="ui_ccnotepad.h" line="1417"/>
        <source>Fortran</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1050"/>
        <location filename="ui_ccnotepad.h" line="1418"/>
        <source>Shell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1058"/>
        <location filename="ui_ccnotepad.h" line="1419"/>
        <source>ActionScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1066"/>
        <location filename="ui_ccnotepad.h" line="1420"/>
        <source>NSIS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1074"/>
        <location filename="ui_ccnotepad.h" line="1421"/>
        <source>Tcl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1082"/>
        <location filename="ui_ccnotepad.h" line="1422"/>
        <source>Lisp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1090"/>
        <location filename="ui_ccnotepad.h" line="1423"/>
        <source>Scheme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1098"/>
        <location filename="ui_ccnotepad.h" line="1424"/>
        <source>Assembly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1111"/>
        <location filename="ui_ccnotepad.h" line="1426"/>
        <source>Diff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1119"/>
        <location filename="ui_ccnotepad.h" line="1427"/>
        <source>Properties file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1127"/>
        <location filename="ui_ccnotepad.h" line="1428"/>
        <source>PostScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1135"/>
        <location filename="ui_ccnotepad.h" line="1429"/>
        <source>Ruby</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1143"/>
        <location filename="ui_ccnotepad.h" line="1430"/>
        <source>Smalltalk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1151"/>
        <location filename="ui_ccnotepad.h" line="1431"/>
        <source>VHDL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1159"/>
        <location filename="ui_ccnotepad.h" line="1432"/>
        <source>AutoIt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1167"/>
        <location filename="ui_ccnotepad.h" line="1433"/>
        <source>CMake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1175"/>
        <location filename="ui_ccnotepad.h" line="1434"/>
        <source>PowerShell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1183"/>
        <location filename="ui_ccnotepad.h" line="1435"/>
        <source>Jsp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1191"/>
        <location filename="ui_ccnotepad.h" line="1436"/>
        <source>CoffeeScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1199"/>
        <location filename="ui_ccnotepad.h" line="1437"/>
        <source>BaanC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1207"/>
        <location filename="ui_ccnotepad.h" line="1438"/>
        <source>S-Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1215"/>
        <location filename="ui_ccnotepad.h" line="1439"/>
        <source>TypeScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1223"/>
        <location filename="ui_ccnotepad.h" line="1440"/>
        <source>Visual Prolog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1231"/>
        <location filename="ui_ccnotepad.h" line="1441"/>
        <source>Txt2tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1239"/>
        <location filename="ui_ccnotepad.h" line="1442"/>
        <source>Rust</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1247"/>
        <location filename="ui_ccnotepad.h" line="1443"/>
        <source>Registry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1255"/>
        <location filename="ui_ccnotepad.h" line="1444"/>
        <source>REBOL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1263"/>
        <location filename="ui_ccnotepad.h" line="1445"/>
        <source>OScript</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1271"/>
        <location filename="ui_ccnotepad.h" line="1446"/>
        <source>Nncrontab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1279"/>
        <location filename="ui_ccnotepad.h" line="1447"/>
        <source>Nim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1287"/>
        <location filename="ui_ccnotepad.h" line="1448"/>
        <source>MMIXAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1295"/>
        <location filename="ui_ccnotepad.h" line="1449"/>
        <source>LaTex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1303"/>
        <location filename="ui_ccnotepad.h" line="1450"/>
        <source>Forth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1311"/>
        <location filename="ui_ccnotepad.h" line="1451"/>
        <source>ESCRIPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1319"/>
        <location filename="ui_ccnotepad.h" line="1452"/>
        <source>Erlang</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1327"/>
        <location filename="ui_ccnotepad.h" line="1453"/>
        <source>Csound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1335"/>
        <location filename="ui_ccnotepad.h" line="1454"/>
        <source>FreeBasic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1343"/>
        <location filename="ui_ccnotepad.h" line="1455"/>
        <source>BlitzBasic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1351"/>
        <location filename="ui_ccnotepad.h" line="1456"/>
        <source>PureBasic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1359"/>
        <location filename="ui_ccnotepad.h" line="1457"/>
        <source>AviSynth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1367"/>
        <location filename="ui_ccnotepad.h" line="1458"/>
        <source>ASN.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1375"/>
        <location filename="ui_ccnotepad.h" line="1459"/>
        <source>Swift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1383"/>
        <location filename="ui_ccnotepad.h" line="1460"/>
        <source>Intel HEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1391"/>
        <location filename="ui_ccnotepad.h" line="1461"/>
        <source>Fortran77</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1399"/>
        <location filename="ui_ccnotepad.h" line="1462"/>
        <source>Edifact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1404"/>
        <location filename="cceditor/ccnotepad.ui" line="1412"/>
        <location filename="ui_ccnotepad.h" line="1463"/>
        <location filename="ui_ccnotepad.h" line="1464"/>
        <source>MarkDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1420"/>
        <location filename="ui_ccnotepad.h" line="1465"/>
        <source>Octave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1428"/>
        <location filename="ui_ccnotepad.h" line="1466"/>
        <source>Po</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1436"/>
        <location filename="ui_ccnotepad.h" line="1467"/>
        <source>Pov</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1444"/>
        <location filename="ui_ccnotepad.h" line="1468"/>
        <source>json</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1460"/>
        <location filename="ui_ccnotepad.h" line="1470"/>
        <source>AVS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1468"/>
        <location filename="ui_ccnotepad.h" line="1471"/>
        <source>Bash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1476"/>
        <location filename="ui_ccnotepad.h" line="1472"/>
        <source>IDL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1484"/>
        <location filename="ui_ccnotepad.h" line="1473"/>
        <source>Matlab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1492"/>
        <location filename="ui_ccnotepad.h" line="1474"/>
        <source>Spice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1500"/>
        <location filename="ui_ccnotepad.h" line="1475"/>
        <source>Verilog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1624"/>
        <location filename="ui_ccnotepad.h" line="1492"/>
        <source>Register</source>
        <translation type="unfinished">注册版本</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1629"/>
        <location filename="ui_ccnotepad.h" line="1493"/>
        <source>Language Format</source>
        <translation type="unfinished">编程语言格式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1634"/>
        <location filename="ui_ccnotepad.h" line="1494"/>
        <source>Open In Text</source>
        <translation type="unfinished">以文本模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1639"/>
        <location filename="ui_ccnotepad.h" line="1495"/>
        <source>Open In Bin</source>
        <translation type="unfinished">以二进制模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1644"/>
        <location filename="ui_ccnotepad.h" line="1496"/>
        <source>Remove Head Blank</source>
        <translation type="unfinished">去除行首空白</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1649"/>
        <location filename="ui_ccnotepad.h" line="1497"/>
        <source>Remove  End Blank</source>
        <translation type="unfinished">去除行尾空白</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1654"/>
        <location filename="ui_ccnotepad.h" line="1498"/>
        <source>Remove Head End Blank</source>
        <translation type="unfinished">去除行首尾空白</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1659"/>
        <location filename="ui_ccnotepad.h" line="1499"/>
        <source>Column Block Editing</source>
        <translation type="unfinished">列块编辑</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1667"/>
        <location filename="ui_ccnotepad.h" line="1500"/>
        <source>Wrap</source>
        <translation type="unfinished">自动换行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1672"/>
        <location filename="ui_ccnotepad.h" line="1501"/>
        <source>Define Language</source>
        <translation type="unfinished">自定义语言</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1677"/>
        <location filename="ui_ccnotepad.h" line="1502"/>
        <source>UPPERCASE</source>
        <translation type="unfinished">转成大写</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1682"/>
        <location filename="ui_ccnotepad.h" line="1503"/>
        <source>lowercase</source>
        <translation type="unfinished">转成小写</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1687"/>
        <location filename="ui_ccnotepad.h" line="1504"/>
        <source>Proper Case</source>
        <translation type="unfinished">每词转成仅首字母大写</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1692"/>
        <location filename="ui_ccnotepad.h" line="1505"/>
        <source>Proper Case (blend)</source>
        <translation type="unfinished">每词的首字母转成大写</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1697"/>
        <location filename="ui_ccnotepad.h" line="1506"/>
        <source>Sentence case</source>
        <translation type="unfinished">每句转成仅首字母大写</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1702"/>
        <location filename="ui_ccnotepad.h" line="1507"/>
        <source>Sentence case (blend)</source>
        <translation type="unfinished">每句的首字母转成大写</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1707"/>
        <location filename="ui_ccnotepad.h" line="1508"/>
        <source>Invert Case</source>
        <translation type="unfinished">大小写互换</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1712"/>
        <location filename="ui_ccnotepad.h" line="1509"/>
        <source>Random Case</source>
        <translation type="unfinished">随机大小写</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1717"/>
        <location filename="ui_ccnotepad.h" line="1510"/>
        <source>Remove Empty Lines</source>
        <translation type="unfinished">移除空行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1722"/>
        <location filename="ui_ccnotepad.h" line="1511"/>
        <source>Remove Empty Lines (Containing Blank characters)</source>
        <translation type="unfinished">移除空行(包括空白字符)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1730"/>
        <location filename="ui_ccnotepad.h" line="1512"/>
        <source>UserDefine</source>
        <translation type="unfinished">用户自定义</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1735"/>
        <location filename="ui_ccnotepad.h" line="1513"/>
        <source>Column Block Mode</source>
        <translation type="unfinished">列块模式...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1740"/>
        <location filename="ui_ccnotepad.h" line="1514"/>
        <source>TAB to Space</source>
        <translation type="unfinished">TAB 转空格</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1745"/>
        <location filename="ui_ccnotepad.h" line="1515"/>
        <source>Space to TAB (All)</source>
        <translation type="unfinished">空格转 TAB (全部)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1750"/>
        <location filename="ui_ccnotepad.h" line="1516"/>
        <source>Space to TAB (Leading)</source>
        <translation type="unfinished">空格转 TAB (行首)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1755"/>
        <location filename="ui_ccnotepad.h" line="1517"/>
        <source>Duplicate Current Line</source>
        <translation type="unfinished">复制当前行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1758"/>
        <location filename="ui_ccnotepad.h" line="1519"/>
        <source>Ctrl+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1763"/>
        <location filename="ui_ccnotepad.h" line="1521"/>
        <source>Remove Duplicate Lines</source>
        <translation type="unfinished">删除重复行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1768"/>
        <location filename="ui_ccnotepad.h" line="1522"/>
        <source>Remove Consecutive Duplicate Lines</source>
        <translation type="unfinished">删除连续的重复行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1773"/>
        <location filename="ui_ccnotepad.h" line="1523"/>
        <source>Split Lines</source>
        <translation type="unfinished">分隔行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1778"/>
        <location filename="ui_ccnotepad.h" line="1524"/>
        <source>Join Lines</source>
        <translation type="unfinished">合并行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1783"/>
        <location filename="ui_ccnotepad.h" line="1525"/>
        <source>Move Up Current Line</source>
        <translation type="unfinished">上移当前行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1786"/>
        <location filename="ui_ccnotepad.h" line="1527"/>
        <source>Ctrl+Shift+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1791"/>
        <location filename="ui_ccnotepad.h" line="1529"/>
        <source>Move Down Current Line</source>
        <translation type="unfinished">下移当前行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1794"/>
        <location filename="ui_ccnotepad.h" line="1531"/>
        <source>Ctrl+Shift+Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1799"/>
        <location filename="ui_ccnotepad.h" line="1533"/>
        <source>Insert Blank Line Above Current</source>
        <translation type="unfinished">在当前行上方插入空行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1802"/>
        <location filename="ui_ccnotepad.h" line="1535"/>
        <source>Ctrl+Alt+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1807"/>
        <location filename="ui_ccnotepad.h" line="1537"/>
        <source>Insert Blank Line Below Current</source>
        <translation type="unfinished">在当前行下方插入空行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1810"/>
        <location filename="ui_ccnotepad.h" line="1539"/>
        <source>Ctrl+Alt+Shift+Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1815"/>
        <location filename="ui_ccnotepad.h" line="1541"/>
        <source>Reverse Line Order</source>
        <translation type="unfinished">反排序行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1820"/>
        <location filename="ui_ccnotepad.h" line="1542"/>
        <source>Randomize Line Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1825"/>
        <location filename="ui_ccnotepad.h" line="1543"/>
        <source>Sort Lines Lexicographically Ascending</source>
        <translation type="unfinished">升序排列文本行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1830"/>
        <location filename="ui_ccnotepad.h" line="1544"/>
        <source>Sort Lines Lex. Ascending Ignoring Case</source>
        <translation type="unfinished">升序排列文本行（不分大小写）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1835"/>
        <location filename="ui_ccnotepad.h" line="1545"/>
        <source>Sort Lines As Integers Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1840"/>
        <location filename="ui_ccnotepad.h" line="1546"/>
        <source>Sort Lines As Decimals (Comma) Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1845"/>
        <location filename="ui_ccnotepad.h" line="1547"/>
        <source>Sort Lines As Decimals (Dot) Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1850"/>
        <location filename="ui_ccnotepad.h" line="1548"/>
        <source>Sort Lines Lexicographically Descending</source>
        <translation type="unfinished">降序排列文本行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1855"/>
        <location filename="ui_ccnotepad.h" line="1549"/>
        <source>Sort Lines Lex. Descending Ignoring Case</source>
        <translation type="unfinished">降序排列文本行（不分大小写）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1860"/>
        <location filename="ui_ccnotepad.h" line="1550"/>
        <source>Sort Lines As Integers Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1865"/>
        <location filename="ui_ccnotepad.h" line="1551"/>
        <source>Sort Lines As Decimals (Comma) Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1870"/>
        <location filename="ui_ccnotepad.h" line="1552"/>
        <source>Sort Lines As Decimals (Dot) Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1875"/>
        <location filename="ui_ccnotepad.h" line="1553"/>
        <source>Find In Dir</source>
        <translation type="unfinished">在目录查找</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1878"/>
        <location filename="ui_ccnotepad.h" line="1555"/>
        <source>Ctrl+Shift+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1956"/>
        <location filename="cceditor/ccnotepad.ui" line="1990"/>
        <location filename="ui_ccnotepad.h" line="1574"/>
        <location filename="ui_ccnotepad.h" line="1579"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1961"/>
        <location filename="ui_ccnotepad.h" line="1575"/>
        <source>Format Xml</source>
        <translation type="unfinished">格式化 Xml</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1966"/>
        <location filename="ui_ccnotepad.h" line="1576"/>
        <source>Format Json</source>
        <translation type="unfinished">格式化 Json</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1974"/>
        <location filename="ui_ccnotepad.h" line="1577"/>
        <source>Dark</source>
        <translation type="unfinished">深色</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1982"/>
        <location filename="ui_ccnotepad.h" line="1578"/>
        <source>VB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1998"/>
        <location filename="ui_ccnotepad.h" line="1580"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="2006"/>
        <location filename="ui_ccnotepad.h" line="1581"/>
        <source>3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="2014"/>
        <location filename="ui_ccnotepad.h" line="1582"/>
        <source>4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="2022"/>
        <location filename="ui_ccnotepad.h" line="1583"/>
        <source>5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="2030"/>
        <location filename="ui_ccnotepad.h" line="1584"/>
        <source>loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="2035"/>
        <location filename="ui_ccnotepad.h" line="1585"/>
        <source>Clear History</source>
        <translation type="unfinished">清除历史打开记录</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1883"/>
        <location filename="ui_ccnotepad.h" line="1557"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1886"/>
        <location filename="ui_ccnotepad.h" line="1559"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1891"/>
        <location filename="ui_ccnotepad.h" line="1561"/>
        <source>Find Prev</source>
        <translation type="unfinished">查找前一个</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1894"/>
        <location filename="ui_ccnotepad.h" line="1563"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1899"/>
        <location filename="ui_ccnotepad.h" line="1565"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1909"/>
        <location filename="ui_ccnotepad.h" line="1567"/>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1917"/>
        <location filename="cceditor/ccnotepad.ui" line="1922"/>
        <location filename="ui_ccnotepad.h" line="1568"/>
        <location filename="ui_ccnotepad.h" line="1569"/>
        <source>Big5</source>
        <translation type="unfinished">Big5(繁体中文）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1930"/>
        <location filename="ui_ccnotepad.h" line="1570"/>
        <source>24x24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1938"/>
        <location filename="ui_ccnotepad.h" line="1571"/>
        <source>36x36</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1946"/>
        <location filename="ui_ccnotepad.h" line="1572"/>
        <source>48x48</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.ui" line="1951"/>
        <location filename="ui_ccnotepad.h" line="1573"/>
        <source>AboutNdd</source>
        <oldsource>About ndd</oldsource>
        <translation type="unfinished">关于Ndd</translation>
    </message>
    <message>
        <source>info</source>
        <translation type="obsolete">信息</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1046"/>
        <location filename="cceditor/ccnotepad.cpp" line="1160"/>
        <source>Ln:0	Col:0</source>
        <translation type="unfinished">行 0 列 0</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1069"/>
        <location filename="cceditor/ccnotepad.cpp" line="1183"/>
        <source>Quit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1532"/>
        <source>Edit with Notepad--</source>
        <oldsource>Edit with Notebook CC</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1605"/>
        <source>Close Current Document</source>
        <translation type="unfinished">关闭当前文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1606"/>
        <source>Close Non-Current documents</source>
        <translation type="unfinished">关闭所有非当前文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1607"/>
        <source>Close Left All</source>
        <translation type="unfinished">关闭左边所有文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1608"/>
        <source>Close Right All</source>
        <translation type="unfinished">关闭右边所有文档</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1612"/>
        <source>Current Document Sava as...</source>
        <translation type="unfinished">当前文件另存为</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1614"/>
        <source>Show File in Explorer...</source>
        <translation type="unfinished">定位到文件路径</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1613"/>
        <source>Open in New Window</source>
        <translation type="unfinished">在新窗口中打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="942"/>
        <source>Can&apos;t Get Admin Auth, Open File %1 failed</source>
        <translation type="unfinished">获取管理员权限失败，打开文件 %1 失败。修改系统文件请以管理员权限执行ndd程序。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1547"/>
        <source>Please run in admin auth</source>
        <translation type="unfinished">请在管理员权限下执行程序</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1611"/>
        <source>Rename Current Document </source>
        <translation type="unfinished">重命名当前文件</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1617"/>
        <source>Reload With Text Mode</source>
        <translation type="unfinished">重新以文本模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1618"/>
        <source>Reload With Hex Mode</source>
        <translation type="unfinished">重新以二进制模式打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1621"/>
        <source>Select Left Cmp File</source>
        <translation type="unfinished">选择为左边对比文件</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1622"/>
        <source>Select Right Cmp File</source>
        <translation type="unfinished">选择为右边对比文件</translation>
    </message>
    <message>
        <source>Big Text File ReadOnly</source>
        <translation type="obsolete">大文本文件只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2077"/>
        <source>New File</source>
        <translation type="unfinished">新建</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2083"/>
        <source>Open File</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2089"/>
        <location filename="cceditor/ccnotepad.cpp" line="3888"/>
        <source>Save File</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2095"/>
        <source>Save All File</source>
        <translation type="unfinished">保存所有</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2102"/>
        <source>Cycle Auto Save</source>
        <translation type="unfinished">周期自动保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2169"/>
        <source>Mark</source>
        <translation type="unfinished">标记</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2178"/>
        <source>word highlight(F8)</source>
        <translation type="unfinished">高亮单词（F8)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2186"/>
        <source>clear all highlight(F7)</source>
        <translation type="unfinished">取消所有高亮（F7)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2196"/>
        <source>Zoom In</source>
        <translation type="unfinished">放大</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2202"/>
        <source>Zoom Out</source>
        <translation type="unfinished">缩小</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2212"/>
        <source>Word Wrap</source>
        <translation type="unfinished">自动换行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2231"/>
        <source>Show Blank</source>
        <translation type="unfinished">显示空白字符</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2249"/>
        <source>Indent Guide</source>
        <translation type="unfinished">缩进参考线</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2257"/>
        <source>Pre Hex Page</source>
        <translation type="unfinished">上一页/位置</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2263"/>
        <source>Next Hex Page</source>
        <translation type="unfinished">下一页/位置</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2269"/>
        <source>Goto Hex Page</source>
        <translation type="unfinished">跳转到文件偏移地址</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2277"/>
        <source>File Compare</source>
        <translation type="unfinished">文件对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2283"/>
        <source>Dir Compare</source>
        <translation type="unfinished">目录对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2289"/>
        <source>Bin Compare</source>
        <translation type="unfinished">二进制对比</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2297"/>
        <source>transform encoding</source>
        <translation type="unfinished">转换编码</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2303"/>
        <source>batch rename file</source>
        <translation type="unfinished">批量重命名</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2706"/>
        <source>Big5(Traditional Chinese)</source>
        <translation type="unfinished">Big5(繁体中文)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3120"/>
        <source>New File Finished [Text Mode] Zoom %1%</source>
        <translation type="unfinished">创建新文件成功 缩放率 %1%</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3404"/>
        <source>File %1 Open Finished [Text Mode] Zoom %2%</source>
        <translation type="unfinished">文件 %1 打开成功 [文本模式] 缩放率 %2%</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4796"/>
        <location filename="cceditor/ccnotepad.cpp" line="4820"/>
        <source>Current Zoom Value is %1%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="6004"/>
        <source>Ndd Version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="6010"/>
        <source>Registered Version</source>
        <translation type="unfinished">注册过的正版软件！（恭喜）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="6014"/>
        <source>Free Trial</source>
        <translation type="unfinished">免费永久试用版本（捐赠可获取注册码）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="6076"/>
        <source>%1 is not a file, skip open it...</source>
        <translation type="unfinished">%1 不是一个文件，跳过打开它......</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="7099"/>
        <location filename="cceditor/ccnotepad.cpp" line="7187"/>
        <location filename="cceditor/ccnotepad.cpp" line="7206"/>
        <source>The ReadOnly document does not allow this operation.</source>
        <translation type="unfinished">当前只读显示文件不允许该操作！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="7161"/>
        <source>Column Edit Mode Tips</source>
        <translation type="unfinished">列块模式提示</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="7161"/>
        <source>&quot;ALT+Mouse Click&quot; or &quot;Alt+Shift+Arrow keys&quot; Switch to mode!</source>
        <translation type="unfinished">请使用&apos;ALT+鼠标点选&apos; 或 &apos;Alt+Shif+箭头键&apos;切换列块模式。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="7601"/>
        <source>SortingError</source>
        <translation type="unfinished">排序错误</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="7601"/>
        <source>Unable to perform numeric sorting due to line %1.</source>
        <translation type="unfinished">行 %1 不能进行排序操作！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="7718"/>
        <source>Xml format error, please check!</source>
        <translation type="unfinished">Xml 格式化错误，请检查文件格式！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="7748"/>
        <source>Json format error, please check!</source>
        <translation type="unfinished">Json 格式化错误，请检查文件格式！</translation>
    </message>
    <message>
        <source>The window background that has been opened will take effect after it is reopened.</source>
        <translation type="obsolete">已打开的窗口背景颜色，将在文件重新打开后才会生效！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2702"/>
        <source>GB18030(Simplified Chinese)</source>
        <translation type="unfinished">GB18030(简体中文)</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2719"/>
        <source>Language: %1</source>
        <translation type="unfinished">语法：%1</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2740"/>
        <source>Reload</source>
        <translation type="unfinished">重加载</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2910"/>
        <source>Yes</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2910"/>
        <location filename="cceditor/ccnotepad.cpp" line="3286"/>
        <source>No</source>
        <translation type="unfinished">放弃修改</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2910"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3045"/>
        <location filename="cceditor/ccnotepad.cpp" line="6830"/>
        <source>Restore Last Temp File %1 Failed</source>
        <translation type="unfinished">恢复临时文件 %1 失败！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3286"/>
        <source>Recover File?</source>
        <translation type="unfinished">是否恢复文件？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3286"/>
        <source>File %1 abnormally closed last time , Restore it ?</source>
        <translation type="unfinished">文件 %1 上次异常退出并留下未保存存档，是否恢复文件存档？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3286"/>
        <source>Restore</source>
        <translation type="unfinished">恢复文件？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3333"/>
        <source>File %1 Open Failed</source>
        <translation type="unfinished">文件 %1 打开失败！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="6931"/>
        <source>File %1 Open Finished [Text Mode]</source>
        <translation type="unfinished">文件 %1 打开成功 [文本模式]</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3441"/>
        <location filename="cceditor/ccnotepad.cpp" line="3522"/>
        <source>Current offset is %1 , load Contens Size is %2, File Total Size is %3</source>
        <translation type="unfinished">当前文件偏移 %1 ， 加载内容大小是 %2，文件总大小是 %3 （字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3576"/>
        <source>File %1 Open Finished [Hex ReayOnly Mode]</source>
        <translation type="unfinished">文件 %1 打开成功 [二进制只读模式]</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3632"/>
        <source>file %1 may be a hex file , try open with text file.</source>
        <translation type="unfinished">文件 %1 可能是二进制文件，尝试以文本格式打开。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3720"/>
        <source>Save File %1 failed. You may not have write privileges 
Please save as a new file!</source>
        <translation type="unfinished">保存文件 %1 失败！ 你可能没有文件写权限，请另存为一个新文件！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4250"/>
        <source>Cycle autosave on ...</source>
        <translation type="unfinished">周期性自动保存文件已开启...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4258"/>
        <source>Cycle autosave off ...</source>
        <translation type="unfinished">周期性自动保存文件已关闭...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4315"/>
        <source>The current document has been automatically saved</source>
        <translation type="unfinished">当前文件周期性自动保存完毕！</translation>
    </message>
    <message>
        <source>file %1 was closed !</source>
        <translation type="obsolete">文件 %1 已经关闭 ！</translation>
    </message>
    <message>
        <source>&quot;%1&quot;

 
This file has been modified by another program.
Do you want to reload it?</source>
        <translation type="obsolete">%1\n\n \n文件已在外部被其它程序修改。\n是否重新加载该文件?</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2757"/>
        <source>Ln: %1	Col: %2</source>
        <translation type="unfinished">行：%1 列：%2</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2910"/>
        <source>Save File?</source>
        <translation type="unfinished">保存文件？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2910"/>
        <source>if save file %1 ?</source>
        <translation type="unfinished">是否保存文件 %1 ？</translation>
    </message>
    <message>
        <source>Current offset is %1 , File Size is %2</source>
        <translation type="obsolete">当前文件偏移 %1 ， 文件大小是 %2 （字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="942"/>
        <location filename="cceditor/ccnotepad.cpp" line="3593"/>
        <location filename="cceditor/ccnotepad.cpp" line="3692"/>
        <location filename="cceditor/ccnotepad.cpp" line="3720"/>
        <location filename="cceditor/ccnotepad.cpp" line="3895"/>
        <location filename="cceditor/ccnotepad.cpp" line="4008"/>
        <location filename="cceditor/ccnotepad.cpp" line="4057"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3593"/>
        <source>file %1 not exist.</source>
        <translation type="unfinished">文件 %1 不存在</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3605"/>
        <location filename="cceditor/ccnotepad.cpp" line="6123"/>
        <source>file %1 already open at tab %2</source>
        <translation type="unfinished">文件 %1 已经在页面 %2 中打开</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3692"/>
        <location filename="cceditor/ccnotepad.cpp" line="3694"/>
        <source>Save File %1 failed. Can not write auth, Please save as new file</source>
        <translation type="unfinished">保存 %1 失败。当前文件没有写权限，请另存为一个新文件</translation>
    </message>
    <message>
        <source>Open File %1 failed</source>
        <translation type="obsolete">打开文件 %1 失败</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3150"/>
        <location filename="cceditor/ccnotepad.cpp" line="3863"/>
        <location filename="cceditor/ccnotepad.cpp" line="4033"/>
        <location filename="cceditor/ccnotepad.cpp" line="5473"/>
        <source>Only Text File Can Use it, Current Doc is a Hex File !</source>
        <translation type="unfinished">只有文本模式才能使用该功能，当前文件是二进制文件！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="2740"/>
        <source>&quot;%1&quot; This file has been modified by another program. Do you want to reload it?</source>
        <translation type="unfinished">%1 该文件已在外部被其它程序修改，是否重新加载？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="918"/>
        <source>Run As Admin Failed to save the file. Please check the file permissions.</source>
        <translation type="unfinished">以管理员模式保存文件失败！请检查文件的权限。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1222"/>
        <source>If display exceptions,Please Install System Font Courier</source>
        <translation type="unfinished">如果界面字体不满意，还请安装windows系统字体 Courier</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1299"/>
        <source>Set/Remove BookMark</source>
        <translation type="unfinished">设置/取消书签</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1302"/>
        <source>Next BookMark</source>
        <translation type="unfinished">下一书签</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1305"/>
        <source>Prev BookMark</source>
        <translation type="unfinished">上一书签</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1308"/>
        <source>ClearAll BookMark</source>
        <translation type="unfinished">清除所有书签</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1311"/>
        <source>Cut BookMark Lines</source>
        <translation type="unfinished">剪切书签行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1314"/>
        <source>Copy BookMark Lines</source>
        <translation type="unfinished">复制书签行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1317"/>
        <source>Paste BookMark Lines</source>
        <translation type="unfinished">粘贴(替换)书签行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1320"/>
        <source>Delete BookMark Lines</source>
        <translation type="unfinished">删除书签行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1323"/>
        <source>Delete UnBookMark Lines</source>
        <translation type="unfinished">删除未标记行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1326"/>
        <source>Clip BookMark</source>
        <translation type="unfinished">反向标记书签</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1354"/>
        <source>Color %1</source>
        <translation type="unfinished">颜色 %1</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1691"/>
        <source>The currently file %1 is already in text mode</source>
        <translation type="unfinished">当前文件 %1 已经是文本模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="1707"/>
        <source>The currently file %1 is already in bin mode</source>
        <translation type="unfinished">当前文件 %1 已经是二进制模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3410"/>
        <location filename="cceditor/ccnotepad.cpp" line="6937"/>
        <source>File %1 Open Finished [Text ReadOnly Mode] (Note: display up to 50K bytes ...)</source>
        <translation type="unfinished">文件 %1 打开成功 [文本只读模式] （乱码：二进制文件强行以文本格式显示，最多显示50K字节的内容，后面忽略...）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3895"/>
        <location filename="cceditor/ccnotepad.cpp" line="4057"/>
        <source>file %1 already open at tab %2, please select other file name.</source>
        <translation type="unfinished">文件 %1 已经存在于页面 %2 中，请选择一个其它名称</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="3969"/>
        <source>Rename File As ...</source>
        <translation type="unfinished">重命名...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4008"/>
        <source>file %1 reanme failed!</source>
        <translation type="unfinished">文件 %1 重命名失败！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4048"/>
        <location filename="cceditor/ccnotepad.cpp" line="4083"/>
        <source>Save File As ...</source>
        <translation type="unfinished">另存为文件 ...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4587"/>
        <source>Close ?</source>
        <translation type="unfinished">关闭?</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="4587"/>
        <source>already has child window open, close all ?</source>
        <translation type="unfinished">目前还有子窗口处于打开状态，关闭所有窗口吗？</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5264"/>
        <source>Find result</source>
        <translation type="unfinished">查找结果</translation>
    </message>
    <message>
        <source>file was closed !</source>
        <translation type="obsolete">文件已关闭</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5393"/>
        <location filename="cceditor/ccnotepad.cpp" line="5404"/>
        <source>Find result - %1 hit</source>
        <translation type="unfinished">查找结果 - %1 命中</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5505"/>
        <source>Convert end of line In progress, please wait ...</source>
        <translation type="unfinished">行尾转换中，请等待...</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5510"/>
        <source>Convert end of line finish.</source>
        <translation type="unfinished">行尾转换完毕</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5534"/>
        <source>Go to line</source>
        <translation type="unfinished">跳转到行</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5534"/>
        <source>Line Num:</source>
        <translation type="unfinished">行号</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5810"/>
        <source>no more pre pos</source>
        <translation type="unfinished">没有前一个位置了</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5847"/>
        <source>no more next pos</source>
        <translation type="unfinished">没有后一个位置了</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5865"/>
        <location filename="cceditor/ccnotepad.cpp" line="5882"/>
        <source>The Last Page ! Current offset is %1 , load Contens Size is %2, File Total Size is %3</source>
        <translation type="unfinished">最后一页！当前文件偏移是 %1 ，加载内容大小是 %2 ，文件总大小是 %3 (字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5920"/>
        <source>Only Hex File Can Use it, Current Doc not a Hex File !</source>
        <translation type="unfinished">只有二进制文件具备该功能。当前文件不是二进制文件！</translation>
    </message>
    <message>
        <source>The Last Page ! Current offset is %1 , File Size is %2</source>
        <translation type="obsolete">最后一页！当前文件偏移是 %1 ，文件大小是 %2 (字节）</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5352"/>
        <source>file %1 was not exists !</source>
        <translation type="unfinished">文件 %1 不存在！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5936"/>
        <location filename="cceditor/ccnotepad.cpp" line="5961"/>
        <source>Error file offset addr , please check !</source>
        <translation type="unfinished">错误的文件偏移量地址，请检查！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5949"/>
        <location filename="cceditor/ccnotepad.cpp" line="5974"/>
        <source>File Size is %1, addr %2 is exceeds file size</source>
        <translation type="unfinished">文件大小是 %1，当前地址 %2 超过了文件大小。</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5980"/>
        <source>Current Text Doc Can Not Use it !</source>
        <oldsource>Current Text Doc Canp Not Use it !</oldsource>
        <translation type="unfinished">当前是常规文本文档，不能使用该功能！</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="5989"/>
        <location filename="cceditor/ccnotepad.cpp" line="5993"/>
        <source>bugfix: https://github.com/cxasm/notepad-- 
china: https://gitee.com/cxasm/notepad--</source>
        <oldsource>bugfix: https://github.com/cxasm/notepad--</oldsource>
        <translation type="unfinished">bug反馈：https://github.com/cxasm/notepad--
国内：https://gitee.com/cxasm/notepad--</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="6445"/>
        <location filename="cceditor/ccnotepad.cpp" line="6479"/>
        <source>notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="6445"/>
        <location filename="cceditor/ccnotepad.cpp" line="6479"/>
        <source>file path not exist, remove recent record!</source>
        <translation type="unfinished">文件路径不存在，删除历史记录！</translation>
    </message>
</context>
<context>
    <name>CTipWin</name>
    <message>
        <location filename="ctipwin.ui" line="30"/>
        <location filename="ui_ctipwin.h" line="63"/>
        <source>Msg Tips</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CmpareMode</name>
    <message>
        <location filename="CmpareMode.cpp" line="219"/>
        <source>File Compare Finished !</source>
        <translation type="unfinished">文件对比完成！</translation>
    </message>
    <message>
        <location filename="CmpareMode.cpp" line="236"/>
        <location filename="CmpareMode.cpp" line="241"/>
        <source>Error : Max Bin File Size is 10M ! Exceeding file size !</source>
        <translation type="unfinished">错误：二进制对比最大文件为10M ! 超过限制。</translation>
    </message>
    <message>
        <location filename="CmpareMode.cpp" line="1535"/>
        <source>File Start Pos Exceeding File Size !</source>
        <translation type="unfinished">文件开始偏移位置超过文件长度！</translation>
    </message>
    <message>
        <location filename="CmpareMode.cpp" line="2893"/>
        <source>BigFile Compare, left linenum %1 , right lineNum %2, Please Waiting !</source>
        <translation type="unfinished">大文件对比，左文件行数 %1 ，右文件行数 %2, 请等待 ！</translation>
    </message>
    <message>
        <source>Compare Left, Total Step %1 Cur Deal step %2 ...</source>
        <translation type="obsolete">对比左文件，总步数 %1 当前处理 %2 步 ...</translation>
    </message>
    <message>
        <source>Compare Right, Total Step %1 Cur Deal step %2 ...</source>
        <translation type="obsolete">对比右文件，总步数 %1 当前处理 %2 步 ...</translation>
    </message>
    <message>
        <source>Outpu Compare Result, Total Step %1 , Current Step %2 ......</source>
        <translation type="obsolete">输出对比结果中，总步数 %1，当前第 %2 步 ......</translation>
    </message>
</context>
<context>
    <name>ColumnEdit</name>
    <message>
        <location filename="columnedit.ui" line="20"/>
        <location filename="ui_columnedit.h" line="274"/>
        <source>ColumnEdit</source>
        <translation type="unfinished">列块编辑</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="44"/>
        <location filename="ui_columnedit.h" line="275"/>
        <source>Insert Text</source>
        <translation type="unfinished">插入文本</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="61"/>
        <location filename="ui_columnedit.h" line="276"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="68"/>
        <location filename="ui_columnedit.h" line="277"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="79"/>
        <location filename="ui_columnedit.h" line="278"/>
        <source>Insert Num</source>
        <translation type="unfinished">插入数字</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="99"/>
        <location filename="ui_columnedit.h" line="279"/>
        <source>Initial value:</source>
        <translation type="unfinished">初始值：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="106"/>
        <location filename="columnedit.ui" line="112"/>
        <location filename="ui_columnedit.h" line="280"/>
        <location filename="ui_columnedit.h" line="281"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="139"/>
        <location filename="ui_columnedit.h" line="282"/>
        <source>increment:</source>
        <translation type="unfinished">增量值：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="179"/>
        <location filename="ui_columnedit.h" line="283"/>
        <source>Repetitions:</source>
        <translation type="unfinished">重复次数：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="219"/>
        <location filename="ui_columnedit.h" line="284"/>
        <source>prefix string:</source>
        <translation type="unfinished">前缀字符串：</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="248"/>
        <location filename="ui_columnedit.h" line="285"/>
        <source>Format</source>
        <translation type="unfinished">格式</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="256"/>
        <location filename="ui_columnedit.h" line="286"/>
        <source>Decimal </source>
        <translation type="unfinished">十进制</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="266"/>
        <location filename="ui_columnedit.h" line="287"/>
        <source>Hex</source>
        <translation type="unfinished">16进制</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="277"/>
        <location filename="ui_columnedit.h" line="288"/>
        <source>Octal </source>
        <translation type="unfinished">八进制</translation>
    </message>
    <message>
        <location filename="columnedit.ui" line="284"/>
        <location filename="ui_columnedit.h" line="289"/>
        <source>Binary</source>
        <translation type="unfinished">二进制</translation>
    </message>
</context>
<context>
    <name>CompareDirs</name>
    <message>
        <location filename="CompareDirs.ui" line="14"/>
        <location filename="ui_CompareDirs.h" line="218"/>
        <source>NetRegister Dirs</source>
        <translation type="unfinished">对比文件夹</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="84"/>
        <location filename="CompareDirs.ui" line="212"/>
        <location filename="ui_CompareDirs.h" line="220"/>
        <location filename="ui_CompareDirs.h" line="232"/>
        <source>Open Dir</source>
        <translatorcomment>打开文件夹</translatorcomment>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="98"/>
        <location filename="CompareDirs.ui" line="226"/>
        <location filename="ui_CompareDirs.h" line="224"/>
        <location filename="ui_CompareDirs.h" line="236"/>
        <source>Reload Dir</source>
        <translation type="unfinished">重载</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="144"/>
        <location filename="CompareDirs.ui" line="266"/>
        <location filename="ui_CompareDirs.h" line="230"/>
        <location filename="ui_CompareDirs.h" line="242"/>
        <source>Name</source>
        <translation type="unfinished">文件名</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="149"/>
        <location filename="CompareDirs.ui" line="271"/>
        <location filename="ui_CompareDirs.h" line="229"/>
        <location filename="ui_CompareDirs.h" line="241"/>
        <source>FileSize</source>
        <translation type="unfinished">大小</translation>
    </message>
    <message>
        <location filename="CompareDirs.ui" line="154"/>
        <location filename="CompareDirs.ui" line="276"/>
        <location filename="ui_CompareDirs.h" line="228"/>
        <location filename="ui_CompareDirs.h" line="240"/>
        <source>Modify Time</source>
        <oldsource>Time</oldsource>
        <translation type="unfinished">时间</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="87"/>
        <source>rule</source>
        <translation type="unfinished">规则</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="96"/>
        <source>all</source>
        <translation type="unfinished">全部</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="104"/>
        <source>diff</source>
        <translation type="unfinished">不同</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="115"/>
        <source>expand</source>
        <translation type="unfinished">展开</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="123"/>
        <source>fold</source>
        <translation type="unfinished">收起</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="134"/>
        <source>clear</source>
        <translation type="unfinished">清空</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="142"/>
        <source>swap</source>
        <translation type="unfinished">交换</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="150"/>
        <source>reload</source>
        <translation type="unfinished">重载</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="253"/>
        <source>Status: normal</source>
        <translation type="unfinished">状态：正常</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="434"/>
        <location filename="CompareDirs.cpp" line="457"/>
        <source>error: %1 not a dir !</source>
        <translation type="unfinished">错误：%1 不是一个目录！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="609"/>
        <source>now busy, please try later ...</source>
        <translation type="unfinished">当前忙，稍后再试...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="682"/>
        <location filename="CompareDirs.cpp" line="704"/>
        <source>Copy to right</source>
        <translation type="unfinished">拷贝到右边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="683"/>
        <location filename="CompareDirs.cpp" line="705"/>
        <source>Cover Diffent File To Right</source>
        <translation type="unfinished">覆盖不同文件到右边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="684"/>
        <location filename="CompareDirs.cpp" line="706"/>
        <source>Cover Diffent File To Right (Traverse subdirectories)</source>
        <translation type="unfinished">覆盖不同文件到右边(递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="688"/>
        <location filename="CompareDirs.cpp" line="698"/>
        <source>Copy to left</source>
        <translation type="unfinished">拷贝到左边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="689"/>
        <location filename="CompareDirs.cpp" line="699"/>
        <source>Cover Diffent File To Left</source>
        <translation type="unfinished">覆盖不同文件到左边</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="690"/>
        <location filename="CompareDirs.cpp" line="700"/>
        <source>Cover Diffent File To Left (Traverse subdirectories)</source>
        <translation type="unfinished">覆盖不同文件到左边(递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="717"/>
        <source>Mark as equal</source>
        <translation type="unfinished">标记为相等</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="726"/>
        <source>Copy Unique File To Other Side</source>
        <translation type="unfinished">拷贝独有文件到对方</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="736"/>
        <source>Copy Unique File To Other Side (Traverse subdirectories)</source>
        <translation type="unfinished">拷贝独有文件到对方(递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="742"/>
        <source>Delete This File</source>
        <translation type="unfinished">删除该文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="746"/>
        <source>Delete Only in This Side</source>
        <translation type="unfinished">删除本侧独有文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="752"/>
        <source>Cope Path To Clipboard</source>
        <translation type="unfinished">拷贝文件路径到剪切板</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="793"/>
        <source>Find File By Name</source>
        <translation type="unfinished">查找文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="837"/>
        <source>%1 not exist, please check!</source>
        <translation type="unfinished">文件 %1 不存在，请检查！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="838"/>
        <location filename="CompareDirs.cpp" line="876"/>
        <location filename="CompareDirs.cpp" line="1033"/>
        <location filename="CompareDirs.cpp" line="1068"/>
        <location filename="CompareDirs.cpp" line="1138"/>
        <location filename="CompareDirs.cpp" line="1172"/>
        <location filename="CompareDirs.cpp" line="1219"/>
        <source>Notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="867"/>
        <source>del file %1 success!</source>
        <translation type="unfinished">删除文件 %1 成功！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="871"/>
        <source>del file %1 failed, maybe other place using !</source>
        <translation type="unfinished">删除文件 %1 失败，可能其它地方在使用中！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="876"/>
        <source>Do you want to delete this files ?</source>
        <translation type="unfinished">您确定删除该文件吗？</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="915"/>
        <source>right Dirs No Find Prev!</source>
        <translation type="unfinished">右边目录没有找到前一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="916"/>
        <source>right Dirs No Find Next!</source>
        <translation type="unfinished">右边目录没有找到下一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="946"/>
        <location filename="CompareDirs.cpp" line="989"/>
        <location filename="CompareDirs.cpp" line="1018"/>
        <source>Not Find</source>
        <translation type="unfinished">没有找到</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="916"/>
        <source>left Dirs No Find Next!</source>
        <translation type="unfinished">左边目录没有找到下一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="915"/>
        <source>left Dirs No Find Prev!</source>
        <translation type="unfinished">左边目录没有找到上一个！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="946"/>
        <source>can not find %1</source>
        <translation type="unfinished">没有找到相关文件 %1</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1033"/>
        <source>Do you want to overwrite all files (excluding folders) to the other side?</source>
        <translation type="unfinished">您确定覆盖目录下所有不同文件到对方吗？（不递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1053"/>
        <location filename="CompareDirs.cpp" line="1121"/>
        <source>cover file %1 please waiting</source>
        <translation type="unfinished">覆盖文件 %1 请等待</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1059"/>
        <location filename="CompareDirs.cpp" line="1129"/>
        <location filename="CompareDirs.cpp" line="1163"/>
        <location filename="CompareDirs.cpp" line="1209"/>
        <source>cover file finish, total cover %1 files</source>
        <translation type="unfinished">覆盖文件完成，一共覆盖 %1 个文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1068"/>
        <source>Do you want to overwrite all files (Traverse subdirs) to the other side?</source>
        <translation type="unfinished">您确定覆盖目录下所有不同文件到对方吗？（递归覆盖子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1138"/>
        <source>Do you want to copy unique files (excluding folders) to the other side?</source>
        <translation type="unfinished">您确定拷贝此目录下独有文件到对方？（不递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1172"/>
        <source>Do you want to copy unique files (Traverse subdirs) to the other side?</source>
        <translation type="unfinished">您确定拷贝此目录下独有文件到对方？（递归拷贝子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1219"/>
        <source>Do you want to delete all files (excluding folders) only in this side?</source>
        <translation type="unfinished">您确定删除目录下的独有文件吗？（不递归子目录）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1245"/>
        <source>delete file finish, total del %1 files</source>
        <translation type="unfinished">删除文件完成，一共删除 %1 个文件</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1327"/>
        <location filename="CompareDirs.cpp" line="1411"/>
        <location filename="CompareDirs.cpp" line="1501"/>
        <source>%1 not exist, skip ...</source>
        <translation type="unfinished">文件 %1 不存在，跳过...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1336"/>
        <location filename="CompareDirs.cpp" line="1417"/>
        <location filename="CompareDirs.cpp" line="1508"/>
        <source>%1 is exist, if replace ?</source>
        <translation type="unfinished">%1 已经存在，是否替换文件？</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1338"/>
        <location filename="CompareDirs.cpp" line="1419"/>
        <location filename="CompareDirs.cpp" line="1510"/>
        <source>Replace ?</source>
        <translation type="unfinished">是否替换？</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1393"/>
        <location filename="CompareDirs.cpp" line="1493"/>
        <location filename="CompareDirs.cpp" line="1565"/>
        <source>copy file %1 failed, please check file auth !</source>
        <translation type="unfinished">拷贝文件 %1 失败，请检查文件权限！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1581"/>
        <location filename="CompareDirs.cpp" line="1592"/>
        <source>current file: %1</source>
        <oldsource>current file %1</oldsource>
        <translation type="unfinished">当前文件：%1</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2104"/>
        <source>current exec rule mode is quick mode, please wait ...</source>
        <translation type="unfinished">当前执行的对比模式是快速模式，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2108"/>
        <source>current exec rule mode is deep slow mode, please wait ...</source>
        <translation type="unfinished">当前执行的对比模式是深入文本慢速模式，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2142"/>
        <source>There are still %1 files haven&apos;t returned comparison results</source>
        <translation type="unfinished">还有 %1 个文件正在对比中</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2146"/>
        <source>file is %1 in comparing !</source>
        <translation type="unfinished">文件 %1 对比进行中！</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2889"/>
        <source>load dir file tree in progress
, please wait ...</source>
        <translation type="unfinished">正在加载文件树目录，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2918"/>
        <location filename="CompareDirs.cpp" line="2926"/>
        <source>skip dir %1</source>
        <translation type="unfinished">跳过目录 %1</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2949"/>
        <source>load %1 dir %2</source>
        <translation type="unfinished">加载第 %1 个目录 %2</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2986"/>
        <source>skip file ext %1</source>
        <translation type="unfinished">跳过文件类型 %1</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2992"/>
        <source>skip file prefix %1</source>
        <translation type="unfinished">跳过文件前缀 %1</translation>
    </message>
    <message>
        <source>file [%1] not a text file, can&apos;t cmpare !!!</source>
        <translation type="obsolete">文件 [%1] 不是文本文件，不能进行比较！！！</translation>
    </message>
    <message>
        <source>file [%1] may be not a text file, cmp is dangerous!
 Forced comparison ?</source>
        <translation type="obsolete">文件 [%1] 可能不是文本文件，比较操作危险，强行比较吗？（不建议）</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1971"/>
        <location filename="CompareDirs.cpp" line="1988"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2052"/>
        <source>init dir file tree in progress
total %1 file, please wait ...</source>
        <translation type="unfinished">初始化文件树目录中，一共 %1 文件，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2018"/>
        <location filename="CompareDirs.cpp" line="2041"/>
        <location filename="CompareDirs.cpp" line="2100"/>
        <source>Comparison in progress, please wait ...</source>
        <translation type="unfinished">对比文件进行中，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="1640"/>
        <location filename="CompareDirs.cpp" line="1668"/>
        <location filename="CompareDirs.cpp" line="1833"/>
        <location filename="CompareDirs.cpp" line="1861"/>
        <source>file [%1] may be not a text file, cmp in hex mode?</source>
        <translation type="unfinished">文件 %1 可能不是文本格式，是否进行二进制格式对比？</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2127"/>
        <location filename="CompareDirs.cpp" line="2128"/>
        <location filename="CompareDirs.cpp" line="2472"/>
        <location filename="CompareDirs.cpp" line="3023"/>
        <source>compare canceled ...</source>
        <translation type="unfinished">对比被取消</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2169"/>
        <source>compare not finished, user canceled ...</source>
        <translation type="unfinished">对比没有完成，用户取消</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2170"/>
        <location filename="CompareDirs.cpp" line="2255"/>
        <source>user canceled finished ...</source>
        <translation type="unfinished">用户取消完成</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2176"/>
        <location filename="CompareDirs.cpp" line="2259"/>
        <location filename="CompareDirs.cpp" line="2260"/>
        <source>compare file finish ...</source>
        <translation type="unfinished">文件对比完成</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2284"/>
        <source>load dir files, please wait ...</source>
        <translation type="unfinished">加载目录中，请等待...</translation>
    </message>
    <message>
        <location filename="CompareDirs.cpp" line="2336"/>
        <source>load dir finish, total %1 files</source>
        <translation type="unfinished">加载目录完成，一共加载 %1 个文件</translation>
    </message>
</context>
<context>
    <name>CompareHexWin</name>
    <message>
        <location filename="comparehexwin.ui" line="20"/>
        <location filename="ui_comparehexwin.h" line="207"/>
        <source>NetRegister Bin File</source>
        <translation type="unfinished">二进制文件对比</translation>
    </message>
    <message>
        <location filename="comparehexwin.ui" line="81"/>
        <location filename="comparehexwin.ui" line="180"/>
        <location filename="ui_comparehexwin.h" line="209"/>
        <location filename="ui_comparehexwin.h" line="217"/>
        <source>Open File</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="comparehexwin.ui" line="104"/>
        <location filename="comparehexwin.ui" line="203"/>
        <location filename="ui_comparehexwin.h" line="213"/>
        <location filename="ui_comparehexwin.h" line="221"/>
        <source>Save File</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="50"/>
        <location filename="comparehexwin.cpp" line="51"/>
        <source>info</source>
        <translation type="unfinished">信息</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="59"/>
        <location filename="comparehexwin.cpp" line="60"/>
        <source>rule</source>
        <translation type="unfinished">规则</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="68"/>
        <source>clear</source>
        <translation type="unfinished">清空</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="79"/>
        <location filename="comparehexwin.cpp" line="80"/>
        <source>swap</source>
        <translation type="unfinished">交换</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="88"/>
        <location filename="comparehexwin.cpp" line="89"/>
        <source>reload</source>
        <translation type="unfinished">重载</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="178"/>
        <source>Drag file support ...</source>
        <translation type="unfinished">支持文件拖动...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="454"/>
        <source>Comparison in progress, please wait ...</source>
        <translation type="unfinished">对比文件进行中，请等待...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="469"/>
        <source>Error : Max Bin File Size is 10M ! Exceeding file size !</source>
        <translation type="unfinished">错误：二进制对比最大文件为10M ! 超过限制。</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="498"/>
        <source>cmpare bin file in progress
please wait ...</source>
        <translation type="unfinished">二进制文件对比中，请等待...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="624"/>
        <source>now busy, please try later ...</source>
        <translation type="unfinished">当前忙，稍后再试...</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="663"/>
        <location filename="comparehexwin.cpp" line="668"/>
        <source>Compare Result</source>
        <translation type="unfinished">对比结果</translation>
    </message>
    <message>
        <location filename="comparehexwin.cpp" line="663"/>
        <location filename="comparehexwin.cpp" line="668"/>
        <source>Left size %1 byte, right size %2 byte 
Equal content size %3 
Left Equal ratio %4 Right Equal ratio %5</source>
        <translation type="unfinished">左文件大小 %1 右文件大小 %2
相等内容长度 %3 
左边相等率 %4 右边相等率%5</translation>
    </message>
</context>
<context>
    <name>CompareWin</name>
    <message>
        <location filename="comparewin.ui" line="20"/>
        <location filename="ui_comparewin.h" line="294"/>
        <source>NetRegister File</source>
        <translation type="unfinished">对比文件</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="100"/>
        <location filename="comparewin.ui" line="296"/>
        <location filename="ui_comparewin.h" line="296"/>
        <location filename="ui_comparewin.h" line="319"/>
        <source>Open File</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="120"/>
        <location filename="comparewin.ui" line="316"/>
        <location filename="comparewin.cpp" line="4490"/>
        <location filename="comparewin.cpp" line="4520"/>
        <location filename="ui_comparewin.h" line="300"/>
        <location filename="ui_comparewin.h" line="323"/>
        <source>Save File</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="151"/>
        <location filename="comparewin.cpp" line="4287"/>
        <location filename="comparewin.cpp" line="4299"/>
        <location filename="ui_comparewin.h" line="303"/>
        <source>left text code</source>
        <translation type="unfinished">左边编码</translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="172"/>
        <location filename="comparewin.ui" line="365"/>
        <location filename="ui_comparewin.h" line="306"/>
        <location filename="ui_comparewin.h" line="329"/>
        <source>UTF16-LE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="177"/>
        <location filename="comparewin.ui" line="370"/>
        <location filename="ui_comparewin.h" line="307"/>
        <location filename="ui_comparewin.h" line="330"/>
        <source>UTF16-BG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="162"/>
        <location filename="comparewin.ui" line="355"/>
        <location filename="ui_comparewin.h" line="304"/>
        <location filename="ui_comparewin.h" line="327"/>
        <source>UTF-8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="167"/>
        <location filename="comparewin.ui" line="360"/>
        <location filename="ui_comparewin.h" line="305"/>
        <location filename="ui_comparewin.h" line="328"/>
        <source>UTF-8 BOM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="182"/>
        <location filename="comparewin.ui" line="375"/>
        <location filename="ui_comparewin.h" line="308"/>
        <location filename="ui_comparewin.h" line="331"/>
        <source>GBK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="187"/>
        <location filename="comparewin.ui" line="380"/>
        <location filename="ui_comparewin.h" line="309"/>
        <location filename="ui_comparewin.h" line="332"/>
        <source>EUC-JP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="192"/>
        <location filename="comparewin.ui" line="385"/>
        <location filename="ui_comparewin.h" line="310"/>
        <location filename="ui_comparewin.h" line="333"/>
        <source>Shift-JIS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="197"/>
        <location filename="comparewin.ui" line="390"/>
        <location filename="ui_comparewin.h" line="311"/>
        <location filename="ui_comparewin.h" line="334"/>
        <source>EUC-KR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="202"/>
        <location filename="comparewin.ui" line="395"/>
        <location filename="ui_comparewin.h" line="312"/>
        <location filename="ui_comparewin.h" line="335"/>
        <source>KOI8-R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="207"/>
        <location filename="comparewin.ui" line="400"/>
        <location filename="ui_comparewin.h" line="313"/>
        <location filename="ui_comparewin.h" line="336"/>
        <source>TSCII</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="212"/>
        <location filename="comparewin.ui" line="405"/>
        <location filename="ui_comparewin.h" line="314"/>
        <location filename="ui_comparewin.h" line="337"/>
        <source>TIS_620</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="217"/>
        <location filename="comparewin.ui" line="410"/>
        <location filename="ui_comparewin.h" line="315"/>
        <location filename="ui_comparewin.h" line="338"/>
        <source>BIG5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="222"/>
        <location filename="comparewin.ui" line="415"/>
        <location filename="ui_comparewin.h" line="316"/>
        <location filename="ui_comparewin.h" line="339"/>
        <source>UNKNOWN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.ui" line="344"/>
        <location filename="comparewin.cpp" line="4286"/>
        <location filename="comparewin.cpp" line="4300"/>
        <location filename="ui_comparewin.h" line="326"/>
        <source>right text code</source>
        <translation type="unfinished">右边编码</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="310"/>
        <location filename="comparewin.cpp" line="311"/>
        <source>white</source>
        <translation type="unfinished">空白</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="318"/>
        <location filename="comparewin.cpp" line="319"/>
        <source>rule</source>
        <translation type="unfinished">规则</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="328"/>
        <location filename="comparewin.cpp" line="329"/>
        <source>break</source>
        <translation type="unfinished">打断</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="338"/>
        <source>pull</source>
        <translation type="unfinished">拉开</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="339"/>
        <source>pull open</source>
        <translation type="unfinished">拉开对比显示</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="351"/>
        <location filename="comparewin.cpp" line="352"/>
        <source>strict</source>
        <translation type="unfinished">严格</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="363"/>
        <location filename="comparewin.cpp" line="364"/>
        <source>ignore</source>
        <translation type="unfinished">忽略</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="374"/>
        <location filename="comparewin.cpp" line="375"/>
        <source>undo</source>
        <translation type="unfinished">撤销</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="385"/>
        <source>pre</source>
        <translation type="unfinished">上一个</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="386"/>
        <source>pre (F3)</source>
        <translation type="unfinished">上一个(F3)</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="395"/>
        <source>next</source>
        <translation type="unfinished">下一个</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="396"/>
        <source>next (F4)</source>
        <translation type="unfinished">下一个(F4)</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="408"/>
        <source>zoomin</source>
        <translation type="unfinished">放大</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="417"/>
        <source>zoomout</source>
        <translation type="unfinished">缩小</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="427"/>
        <source>clear</source>
        <translation type="unfinished">清空</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="428"/>
        <source>clear current compare</source>
        <translation type="unfinished">清空当前对比</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="436"/>
        <source>swap</source>
        <translation type="unfinished">交换</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="907"/>
        <source>Can not save file !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="918"/>
        <source>Save File As ...</source>
        <translation type="unfinished">另存为文件 ...</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="6221"/>
        <source>Diff Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>reload</source>
        <translation type="obsolete">重载</translation>
    </message>
    <message>
        <source>reload (F5)</source>
        <translation type="obsolete">重载(F5)</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="647"/>
        <source>Drag file support ...</source>
        <translation type="unfinished">支持文件拖动...</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="1708"/>
        <location filename="comparewin.cpp" line="3264"/>
        <location filename="comparewin.cpp" line="3698"/>
        <location filename="comparewin.cpp" line="3712"/>
        <location filename="comparewin.cpp" line="5298"/>
        <location filename="comparewin.cpp" line="5681"/>
        <source>current has %1 differents</source>
        <translation type="unfinished">当前有 %1 处不同块</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="3846"/>
        <source>Current mode can not save file !</source>
        <translation type="unfinished">当前模式不能保存文件！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="965"/>
        <location filename="comparewin.cpp" line="3856"/>
        <source>open file %1 failed</source>
        <translation type="unfinished">打开文件 %1 失败</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="1055"/>
        <location filename="comparewin.cpp" line="3944"/>
        <source>save file finished !</source>
        <translation type="unfinished">保存文件成功！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4014"/>
        <location filename="comparewin.cpp" line="4059"/>
        <location filename="comparewin.cpp" line="4395"/>
        <location filename="comparewin.cpp" line="4428"/>
        <source>The left document has been modified.</source>
        <translation type="unfinished">左边文档已经被修改</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4018"/>
        <location filename="comparewin.cpp" line="4055"/>
        <location filename="comparewin.cpp" line="4399"/>
        <location filename="comparewin.cpp" line="4424"/>
        <source>The right document has been modified.</source>
        <translation type="unfinished">右边文档已经被修改</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4020"/>
        <location filename="comparewin.cpp" line="4061"/>
        <location filename="comparewin.cpp" line="4401"/>
        <location filename="comparewin.cpp" line="4430"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">是否保存修改？</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4023"/>
        <location filename="comparewin.cpp" line="4064"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4024"/>
        <location filename="comparewin.cpp" line="4065"/>
        <source>Discard</source>
        <translation type="unfinished">放弃修改</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4025"/>
        <location filename="comparewin.cpp" line="4066"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4126"/>
        <location filename="comparewin.cpp" line="4209"/>
        <source>no more unequal block!</source>
        <translation type="unfinished">没有更多不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4154"/>
        <source>the first one!</source>
        <translation type="unfinished">第一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4167"/>
        <location filename="comparewin.cpp" line="4250"/>
        <source>the %1 diff, total %2 diff</source>
        <translation type="unfinished">第 %1 处不同， 一共 %2 不同</translation>
    </message>
    <message>
        <source>the %1 diff,total %2 diff</source>
        <translation type="obsolete">第 %1 处不同，一共 %2 处不同。</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4174"/>
        <source>already the first one!</source>
        <translation type="unfinished">已经是第一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4237"/>
        <source>the last one!</source>
        <translation type="unfinished">最后一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4256"/>
        <source>already the last one!</source>
        <translation type="unfinished">已经是最后一个不等块！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="965"/>
        <location filename="comparewin.cpp" line="3846"/>
        <location filename="comparewin.cpp" line="3856"/>
        <location filename="comparewin.cpp" line="4753"/>
        <location filename="comparewin.cpp" line="5148"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="437"/>
        <source>swap left Right windows</source>
        <translation type="unfinished">交换左右窗口位置</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="445"/>
        <source>refresh</source>
        <translation type="unfinished">刷新</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="446"/>
        <source>compare again (F5)</source>
        <translation type="unfinished">重新对比</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="457"/>
        <source>status</source>
        <translation type="unfinished">差异图</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="458"/>
        <source>show diff views</source>
        <translation type="unfinished">显示差异界面</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="4753"/>
        <location filename="comparewin.cpp" line="5148"/>
        <source>The current comparison has encountered an error.Quit temporarily.</source>
        <translation type="unfinished">当前对比发生未知错误，暂时退出。</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="5101"/>
        <source>has %1 differents</source>
        <translation type="unfinished">有 %1 处不同</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="839"/>
        <location filename="comparewin.cpp" line="5442"/>
        <source>Comparison in progress, please wait ...</source>
        <translation type="unfinished">对比文件进行中，请等待...</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="5469"/>
        <location filename="comparewin.cpp" line="5477"/>
        <source>file [%1] maybe not a text file, forse cmpare?(dangerous, may be core)</source>
        <translation type="unfinished">文件 [%1] 可能不是文本,强行使用文本对比?(危险操作)</translation>
    </message>
    <message>
        <source>file [%1] not a text file, can&apos;t cmpare !!!</source>
        <translation type="obsolete">文件 [%1] 不是文本文件，不能进行比较！！！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="5637"/>
        <source>no more undo operator!</source>
        <translation type="unfinished">没有更多撤销！</translation>
    </message>
    <message>
        <location filename="comparewin.cpp" line="6157"/>
        <source>now busy, please try later ...</source>
        <translation type="unfinished">当前忙，稍后再试...</translation>
    </message>
</context>
<context>
    <name>CompareWorker</name>
    <message>
        <location filename="compareworker.ui" line="14"/>
        <location filename="ui_compareworker.h" line="39"/>
        <source>CompareWorker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DiffStatusWinClass</name>
    <message>
        <location filename="diffstatuswin.ui" line="26"/>
        <location filename="ui_diffstatuswin.h" line="55"/>
        <source>DIFF</source>
        <translation type="unfinished">差异点</translation>
    </message>
</context>
<context>
    <name>DirCmpExtWin</name>
    <message>
        <location filename="dircmpextwin.ui" line="14"/>
        <location filename="ui_dircmpextwin.h" line="183"/>
        <source>DirCmpExtWin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="20"/>
        <location filename="ui_dircmpextwin.h" line="184"/>
        <source>Cmp Mode</source>
        <translation type="unfinished">对比模式</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="36"/>
        <location filename="ui_dircmpextwin.h" line="186"/>
        <source>Compare file times and sizes (Fast mode)</source>
        <translation type="unfinished">对比文件大小和时间（快速模式）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="26"/>
        <location filename="ui_dircmpextwin.h" line="185"/>
        <source>Compare files in depth(slow mode,Accurate results)</source>
        <oldsource>Compare files in depth(slow mode)</oldsource>
        <translation type="unfinished">深度对比文件文本模式（慢速模式，结果更精确）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="49"/>
        <location filename="ui_dircmpextwin.h" line="187"/>
        <source>Dir Cmp Options</source>
        <translation type="unfinished">文件夹对比选项</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="72"/>
        <location filename="ui_dircmpextwin.h" line="190"/>
        <source>Compare Hide Dirs</source>
        <translation type="unfinished">对比隐藏目录</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="62"/>
        <location filename="ui_dircmpextwin.h" line="189"/>
        <source>Compare All Files</source>
        <translation type="unfinished">对比所有文件</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="55"/>
        <location filename="ui_dircmpextwin.h" line="188"/>
        <source>Compare Support Ext Files</source>
        <translation type="unfinished">对比已知后缀的文件</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="82"/>
        <location filename="ui_dircmpextwin.h" line="191"/>
        <source>Skip Dirs</source>
        <translation type="unfinished">跳过目录</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="100"/>
        <location filename="ui_dircmpextwin.h" line="192"/>
        <source>Skip Load these Dirs(Separated by:)</source>
        <translation type="unfinished">对比时跳过如下目录（以:号分割）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="107"/>
        <location filename="ui_dircmpextwin.h" line="193"/>
        <source>.svn:.vs:debug:Debug:release:Release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="120"/>
        <location filename="ui_dircmpextwin.h" line="194"/>
        <source>Skip File Exts</source>
        <translation type="unfinished">跳过文件类型</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="138"/>
        <location filename="ui_dircmpextwin.h" line="195"/>
        <source>Skip Cmpare these File Exts(Separated by:)</source>
        <translation type="unfinished">对比时跳过以下文件类型（以:号分割）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="145"/>
        <location filename="ui_dircmpextwin.h" line="196"/>
        <source>.sln:.vcxproj</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="158"/>
        <location filename="ui_dircmpextwin.h" line="197"/>
        <source>Skip FileName Prefix</source>
        <translation type="unfinished">跳过文件前缀</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="176"/>
        <location filename="ui_dircmpextwin.h" line="198"/>
        <source>Skip Cmpare these FileName Prefix(Separated by:)</source>
        <translation type="unfinished">对比时跳过以下文件名前缀（以:号分割）</translation>
    </message>
    <message>
        <location filename="dircmpextwin.ui" line="183"/>
        <location filename="ui_dircmpextwin.h" line="199"/>
        <source>ui_</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DirFindFile</name>
    <message>
        <location filename="dirfindfile.ui" line="26"/>
        <location filename="ui_dirfindfile.h" line="144"/>
        <source>DirFindFile</source>
        <translation type="unfinished">查找文件</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="36"/>
        <location filename="ui_dirfindfile.h" line="145"/>
        <source>Find Options</source>
        <translation type="unfinished">查找选项</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="44"/>
        <location filename="ui_dirfindfile.h" line="146"/>
        <source>Find File Name</source>
        <translation type="unfinished">查找文件名</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="58"/>
        <location filename="ui_dirfindfile.h" line="147"/>
        <source>find in left</source>
        <translation type="unfinished">在左边查找</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="68"/>
        <location filename="ui_dirfindfile.h" line="148"/>
        <source>find in right</source>
        <translation type="unfinished">在右边查找</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="75"/>
        <location filename="ui_dirfindfile.h" line="149"/>
        <source>case sensitive</source>
        <translation type="unfinished">区分大小写</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="92"/>
        <location filename="ui_dirfindfile.h" line="150"/>
        <source>Find Prev</source>
        <translation type="unfinished">查找前一个</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="99"/>
        <location filename="ui_dirfindfile.h" line="151"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="dirfindfile.ui" line="106"/>
        <location filename="ui_dirfindfile.h" line="152"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>DocTypeListView</name>
    <message>
        <location filename="doctypelistview.ui" line="14"/>
        <location filename="ui_doctypelistview.h" line="135"/>
        <source>DocTypeListView</source>
        <translation type="unfinished">文件关联列表</translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="22"/>
        <location filename="ui_doctypelistview.h" line="136"/>
        <source>Support file </source>
        <translation type="unfinished">支持文件类型</translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="71"/>
        <location filename="ui_doctypelistview.h" line="137"/>
        <source>-&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="78"/>
        <location filename="ui_doctypelistview.h" line="138"/>
        <source>&lt;-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="doctypelistview.ui" line="100"/>
        <location filename="ui_doctypelistview.h" line="139"/>
        <source>Custom extension</source>
        <translation type="unfinished">用户扩展类型</translation>
    </message>
    <message>
        <source>support file </source>
        <translation type="obsolete">支持文件后缀</translation>
    </message>
    <message>
        <location filename="doctypelistview.cpp" line="250"/>
        <source>input file ext()</source>
        <translation type="unfinished">输入文件后缀</translation>
    </message>
    <message>
        <location filename="doctypelistview.cpp" line="250"/>
        <source>ext (Split With :)</source>
        <translation type="unfinished">后缀（用:号分割开）</translation>
    </message>
</context>
<context>
    <name>DonateClass</name>
    <message>
        <location filename="donate.ui" line="32"/>
        <location filename="ui_donate.h" line="115"/>
        <source>Donate Me</source>
        <translation type="unfinished">捐赠作者</translation>
    </message>
    <message>
        <location filename="donate.ui" line="112"/>
        <location filename="ui_donate.h" line="117"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donation Software Development By WeChat &lt;/p&gt;&lt;p&gt;Busy living, no time to improve software&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">现实生活中的一地鸡毛
让我无法投入更多时间完善免费软件
请通过微信扫码捐赠作者</translation>
    </message>
</context>
<context>
    <name>EncodeConvert</name>
    <message>
        <location filename="encodeconvert.ui" line="14"/>
        <location filename="ui_encodeconvert.h" line="190"/>
        <source>EncodeConvert</source>
        <translation type="unfinished">文本编码转换</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="40"/>
        <location filename="ui_encodeconvert.h" line="196"/>
        <source>filePath</source>
        <translation type="unfinished">文件路径</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="45"/>
        <location filename="ui_encodeconvert.h" line="195"/>
        <source>file size</source>
        <translation type="unfinished">文件大小</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="50"/>
        <location filename="ui_encodeconvert.h" line="194"/>
        <source>file code</source>
        <translation type="unfinished">文件编码</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="55"/>
        <location filename="ui_encodeconvert.h" line="193"/>
        <source>convert code</source>
        <translation type="unfinished">转换为编码</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="60"/>
        <location filename="ui_encodeconvert.h" line="192"/>
        <source>convert result</source>
        <translation type="unfinished">转换结果</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="75"/>
        <location filename="ui_encodeconvert.h" line="197"/>
        <source>convert options</source>
        <translation type="unfinished">转换选项</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="83"/>
        <location filename="ui_encodeconvert.h" line="198"/>
        <source>convert to code</source>
        <translation type="unfinished">转换为编码</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="97"/>
        <location filename="ui_encodeconvert.h" line="199"/>
        <source>UTF8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="102"/>
        <location filename="ui_encodeconvert.h" line="200"/>
        <source>UTF8 BOM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="117"/>
        <location filename="ui_encodeconvert.h" line="203"/>
        <source>GBK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="107"/>
        <location filename="ui_encodeconvert.h" line="201"/>
        <source>UTF16-LE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="112"/>
        <location filename="ui_encodeconvert.h" line="202"/>
        <source>UTF16-BE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="129"/>
        <location filename="ui_encodeconvert.h" line="205"/>
        <source>deal file ext</source>
        <translation type="unfinished">处理文件类型</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="143"/>
        <location filename="ui_encodeconvert.h" line="206"/>
        <source>all support file ext</source>
        <translation type="unfinished">所有支持的文件类型</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="151"/>
        <location filename="ui_encodeconvert.h" line="209"/>
        <source>user defined</source>
        <translation type="unfinished">自定义</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="154"/>
        <location filename="ui_encodeconvert.h" line="211"/>
        <source>defined</source>
        <translation type="unfinished">自定义</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="181"/>
        <location filename="ui_encodeconvert.h" line="212"/>
        <source>select dir</source>
        <translation type="unfinished">选择目录</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="188"/>
        <location filename="ui_encodeconvert.h" line="213"/>
        <source>start</source>
        <translation type="unfinished">开始转换</translation>
    </message>
    <message>
        <location filename="encodeconvert.ui" line="198"/>
        <location filename="ui_encodeconvert.h" line="214"/>
        <source>close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="69"/>
        <source>&amp;Show File in Explorer...</source>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="99"/>
        <source>input file ext()</source>
        <translation type="unfinished">输入文件后缀</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="99"/>
        <source>ext (Split With :)</source>
        <translation type="unfinished">后缀（用:号分割开）</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="130"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="455"/>
        <source>start scan file text code, please wait...</source>
        <translation type="unfinished">开始扫描文件编码，请等待...</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="475"/>
        <location filename="encodeconvert.cpp" line="581"/>
        <source>ignore</source>
        <translation type="unfinished">忽略</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="489"/>
        <source>please wait, total file %1,cur scan index %2, scan finish %3%</source>
        <translation type="unfinished">请等待，一共 %1个文件，当前扫描第 %2 个，扫描完成率 %3%</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="493"/>
        <source>scan finished, total file %1</source>
        <translation type="unfinished">扫描完成，一共%1个文件</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="576"/>
        <source>already %1 ignore</source>
        <translation type="unfinished">已经是 %1编码,忽略</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="595"/>
        <source>total file %1,cur deal index %2,finish %3%</source>
        <translation type="unfinished">一共 %1 个文件，当前处理第 %2 个，完成率%3%</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="600"/>
        <source>total file %1,cur deal index %2,finish 100%</source>
        <translation type="unfinished">一共 %1 个 文件，当前处理第 %2 个，完成率100%</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="601"/>
        <source>convert finished !</source>
        <translation type="unfinished">转换完成！</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="622"/>
        <source>convert finish</source>
        <translation type="unfinished">转换完成</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="626"/>
        <source>convert fail</source>
        <translation type="unfinished">转换失败</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="627"/>
        <source>file %1 convert failed,pleas check...</source>
        <translation type="unfinished">文件 %1 转换编码失败，请检查...</translation>
    </message>
    <message>
        <location filename="encodeconvert.cpp" line="698"/>
        <source>please drop a file dir ...</source>
        <translation type="unfinished">请拖入一个文件夹...</translation>
    </message>
</context>
<context>
    <name>FileCmpRuleWin</name>
    <message>
        <location filename="filecmprulewin.ui" line="20"/>
        <location filename="ui_filecmprulewin.h" line="160"/>
        <source>FileCmpRuleWin</source>
        <translation type="unfinished">文件对比规则</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="30"/>
        <location filename="ui_filecmprulewin.h" line="161"/>
        <source>Compare Options</source>
        <translation type="unfinished">对比选项</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="36"/>
        <location filename="ui_filecmprulewin.h" line="162"/>
        <source>Ignore whitespace characters before line</source>
        <oldsource>Ignore whitespace characters</oldsource>
        <translation type="unfinished">忽略行前行尾空白字符</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="46"/>
        <location filename="ui_filecmprulewin.h" line="163"/>
        <source>Ignore whitespace characters At back of the line(such as python)</source>
        <translation type="unfinished">只忽略行尾的空白字符（比如python等语言）</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="53"/>
        <location filename="ui_filecmprulewin.h" line="164"/>
        <source>Ignore all whitespace characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="63"/>
        <location filename="ui_filecmprulewin.h" line="165"/>
        <source>Match Options</source>
        <translation type="unfinished">匹配选项</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="69"/>
        <location filename="ui_filecmprulewin.h" line="166"/>
        <source>Blank lines participate in matching</source>
        <translation type="unfinished">空行参与匹配</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="81"/>
        <location filename="ui_filecmprulewin.h" line="167"/>
        <source>Identify matching rates for rows that are equal</source>
        <translation type="unfinished">认定为匹配行的相似率</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="89"/>
        <location filename="ui_filecmprulewin.h" line="168"/>
        <source>Match &gt;= 50%</source>
        <translation type="unfinished">匹配度 &gt;= 50%</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="94"/>
        <location filename="ui_filecmprulewin.h" line="169"/>
        <source>Match &gt;= 70%</source>
        <translation type="unfinished">匹配度 &gt;= 70%</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="99"/>
        <location filename="ui_filecmprulewin.h" line="170"/>
        <source>Match &gt;= 90%</source>
        <translation type="unfinished">匹配度 &gt;= 90%</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="140"/>
        <location filename="ui_filecmprulewin.h" line="172"/>
        <source>Apply</source>
        <translation type="unfinished">确认</translation>
    </message>
    <message>
        <location filename="filecmprulewin.ui" line="147"/>
        <location filename="ui_filecmprulewin.h" line="173"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>FileManager</name>
    <message>
        <location filename="cceditor/filemanager.cpp" line="180"/>
        <location filename="cceditor/filemanager.cpp" line="219"/>
        <location filename="cceditor/filemanager.cpp" line="239"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="180"/>
        <source>Open File %1 failed Can not read auth</source>
        <translation type="unfinished">打开文件 %1 失败。没有读文件的权限。</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="219"/>
        <source>Open File %1 failed</source>
        <translation type="unfinished">打开文件 %1 失败。</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="239"/>
        <source>File is too big to be opened by Notepad--</source>
        <translation type="unfinished">文件太大，不能使用Notepad--打开！</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="263"/>
        <source>The file %1 is likely to be binary. Do you want to open it in binary?</source>
        <translation type="unfinished">文件 %1 可能是二进制格式，你想以二进制（只读）格式打开文件吗？</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="263"/>
        <source>Open with Text or Hex?</source>
        <translation type="unfinished">二进制或文本打开？</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="263"/>
        <source>Hex Open</source>
        <translation type="unfinished">以二进制打开</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="263"/>
        <source>Text Open</source>
        <translation type="unfinished">以文本打开</translation>
    </message>
    <message>
        <location filename="cceditor/filemanager.cpp" line="263"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <source>File is too big to be opened by CC Notepad</source>
        <translation type="obsolete">文件太大不能使用Notepad打开！</translation>
    </message>
</context>
<context>
    <name>FindCmpWin</name>
    <message>
        <location filename="findcmpwin.ui" line="20"/>
        <location filename="ui_findcmpwin.h" line="224"/>
        <source>Find text window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="50"/>
        <location filename="ui_findcmpwin.h" line="239"/>
        <source>find</source>
        <translation type="unfinished">查找</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="63"/>
        <location filename="ui_findcmpwin.h" line="225"/>
        <source>Find what :</source>
        <translation type="unfinished">查找目标：</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="88"/>
        <location filename="ui_findcmpwin.h" line="226"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="94"/>
        <location filename="ui_findcmpwin.h" line="227"/>
        <source>Backward direction</source>
        <translation type="unfinished">反向查找</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="101"/>
        <location filename="ui_findcmpwin.h" line="228"/>
        <source>Match whole word only</source>
        <translation type="unfinished">全词匹配</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="108"/>
        <location filename="ui_findcmpwin.h" line="229"/>
        <source>Match case</source>
        <translation type="unfinished">匹配大小写</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="115"/>
        <location filename="ui_findcmpwin.h" line="230"/>
        <source>Wrap around</source>
        <translation type="unfinished">循环查找</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="128"/>
        <location filename="ui_findcmpwin.h" line="231"/>
        <source>Search Mode</source>
        <translation type="unfinished">查找模式</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="136"/>
        <location filename="ui_findcmpwin.h" line="232"/>
        <source>Regular expression</source>
        <translation type="unfinished">正则表达式</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="145"/>
        <location filename="ui_findcmpwin.h" line="233"/>
        <source>Normal</source>
        <translation type="unfinished">普通</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="162"/>
        <location filename="ui_findcmpwin.h" line="234"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="175"/>
        <location filename="ui_findcmpwin.h" line="235"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="182"/>
        <location filename="ui_findcmpwin.h" line="236"/>
        <source>Diretion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="188"/>
        <location filename="ui_findcmpwin.h" line="237"/>
        <source>Search In Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.ui" line="198"/>
        <location filename="ui_findcmpwin.h" line="238"/>
        <source>Search In Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findcmpwin.cpp" line="168"/>
        <source>what find is null !</source>
        <translation type="unfinished">查找字段为空</translation>
    </message>
    <message>
        <location filename="findcmpwin.cpp" line="186"/>
        <source>cant&apos;t find text &apos;%1&apos;</source>
        <translation type="unfinished">找不到字段 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findcmpwin.cpp" line="202"/>
        <source>no more find text &apos;%1&apos;</source>
        <translation type="unfinished">没有更多的字段 &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>FindResultWin</name>
    <message>
        <location filename="findresultwin.ui" line="20"/>
        <location filename="ui_findresultwin.h" line="63"/>
        <source>FindResultWin</source>
        <translation type="unfinished">查找结果</translation>
    </message>
    <message>
        <source>clear</source>
        <translation type="obsolete">清空</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="61"/>
        <source>clear this find result</source>
        <translation type="unfinished">清除当前结果</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="62"/>
        <source>clear all find result</source>
        <translation type="unfinished">清除所有结果</translation>
    </message>
    <message>
        <source>copy item content</source>
        <translation type="obsolete">拷贝到剪切板</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="70"/>
        <source>copy select item (Ctrl Muli)</source>
        <translation type="unfinished">复制选中项（按ctrl多选）</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="71"/>
        <source>copy select Line (Ctrl Muli)</source>
        <translation type="unfinished">复制选中行（按ctrl多选）</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="65"/>
        <source>select section</source>
        <translation type="unfinished">选中所在节</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="66"/>
        <source>select all item</source>
        <translation type="unfinished">全部选择</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="74"/>
        <source>close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="211"/>
        <location filename="findresultwin.cpp" line="257"/>
        <source>%1 rows selected !</source>
        <translation type="unfinished">%1 行被选中！</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="289"/>
        <source>%1 items have been copied to the clipboard !</source>
        <translation type="unfinished">%1 项已经被复制到剪切板</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="339"/>
        <source>%1 lines have been copied to the clipboard !</source>
        <translation type="unfinished">%1 行已经被复制到剪切板</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="411"/>
        <source>&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;Search &quot;%1&quot; (%2 hits)&lt;/font&gt;</source>
        <translation type="unfinished">&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;查找 &quot;%1&quot; (%2 命中)&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="432"/>
        <location filename="findresultwin.cpp" line="512"/>
        <source>&lt;font style=&apos;font-weight:bold;color:#309730&apos;&gt;%1 (%2 hits)&lt;/font&gt;</source>
        <translation type="unfinished">&lt;font style=&apos;font-weight:bold;color:#309730&apos;&gt;%1 (%2 命中)&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="463"/>
        <source>Line &lt;font style=&apos;color:#ff8040&apos;&gt;%1&lt;/font&gt; : %2</source>
        <translation type="unfinished">行 &lt;font style=&apos;color:#ff8040&apos;&gt;%1&lt;/font&gt; : %2</translation>
    </message>
    <message>
        <location filename="findresultwin.cpp" line="484"/>
        <source>&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;Search &quot;%1&quot; (%2 hits in %3 files)&lt;/font&gt;</source>
        <translation type="unfinished">&lt;font style=&apos;font-weight:bold;color:#343497&apos;&gt;查找 &quot;%1&quot; (%2 命中在 %3 个文件）&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; (%2 hits)</source>
        <translation type="obsolete">查找 &quot;%1&quot; (%2 命中）</translation>
    </message>
    <message>
        <source>%1 (%2 hits)</source>
        <translation type="obsolete">%1 (%2 命中)</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; (%2 hits in %3 files)</source>
        <translation type="obsolete">查找 &quot;%1&quot; (%2 命中在 %3 个文件）</translation>
    </message>
</context>
<context>
    <name>FindWin</name>
    <message>
        <location filename="findwin.ui" line="20"/>
        <location filename="ui_findwin.h" line="960"/>
        <source>MainWindow</source>
        <translation type="unfinished">查找与替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="53"/>
        <location filename="ui_findwin.h" line="988"/>
        <source>find</source>
        <translation type="unfinished">查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="63"/>
        <location filename="findwin.ui" line="306"/>
        <location filename="findwin.ui" line="609"/>
        <location filename="ui_findwin.h" line="961"/>
        <location filename="ui_findwin.h" line="989"/>
        <location filename="ui_findwin.h" line="1008"/>
        <source>Find what :</source>
        <translation type="unfinished">查找目标：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="105"/>
        <location filename="findwin.ui" line="374"/>
        <location filename="ui_findwin.h" line="962"/>
        <location filename="ui_findwin.h" line="991"/>
        <source>Backward direction</source>
        <translation type="unfinished">反向查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="112"/>
        <location filename="findwin.ui" line="381"/>
        <location filename="findwin.ui" line="723"/>
        <location filename="findwin.ui" line="1025"/>
        <location filename="ui_findwin.h" line="963"/>
        <location filename="ui_findwin.h" line="992"/>
        <location filename="ui_findwin.h" line="1014"/>
        <location filename="ui_findwin.h" line="1032"/>
        <source>Match whole word only</source>
        <translation type="unfinished">全词匹配</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="119"/>
        <location filename="findwin.ui" line="388"/>
        <location filename="findwin.ui" line="736"/>
        <location filename="findwin.ui" line="1032"/>
        <location filename="ui_findwin.h" line="964"/>
        <location filename="ui_findwin.h" line="993"/>
        <location filename="ui_findwin.h" line="1015"/>
        <location filename="ui_findwin.h" line="1033"/>
        <source>Match case</source>
        <translation type="unfinished">匹配大小写</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="126"/>
        <location filename="findwin.ui" line="395"/>
        <location filename="ui_findwin.h" line="965"/>
        <location filename="ui_findwin.h" line="994"/>
        <source>Wrap around</source>
        <translation type="unfinished">循环查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="151"/>
        <location filename="findwin.ui" line="420"/>
        <location filename="findwin.ui" line="762"/>
        <location filename="findwin.ui" line="1054"/>
        <location filename="ui_findwin.h" line="966"/>
        <location filename="ui_findwin.h" line="995"/>
        <location filename="ui_findwin.h" line="1016"/>
        <location filename="ui_findwin.h" line="1034"/>
        <source>Search Mode</source>
        <translation type="unfinished">查找模式</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="159"/>
        <location filename="findwin.ui" line="438"/>
        <location filename="findwin.ui" line="780"/>
        <location filename="findwin.ui" line="1062"/>
        <location filename="ui_findwin.h" line="967"/>
        <location filename="ui_findwin.h" line="997"/>
        <location filename="ui_findwin.h" line="1018"/>
        <location filename="ui_findwin.h" line="1035"/>
        <source>Regular expression</source>
        <translation type="unfinished">正则表达式</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="168"/>
        <location filename="findwin.ui" line="426"/>
        <location filename="findwin.ui" line="768"/>
        <location filename="findwin.ui" line="1071"/>
        <location filename="ui_findwin.h" line="968"/>
        <location filename="ui_findwin.h" line="996"/>
        <location filename="ui_findwin.h" line="1017"/>
        <location filename="ui_findwin.h" line="1036"/>
        <source>Normal</source>
        <translation type="unfinished">普通</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="178"/>
        <location filename="findwin.ui" line="447"/>
        <location filename="findwin.ui" line="789"/>
        <location filename="findwin.ui" line="1081"/>
        <location filename="ui_findwin.h" line="969"/>
        <location filename="ui_findwin.h" line="998"/>
        <location filename="ui_findwin.h" line="1019"/>
        <location filename="ui_findwin.h" line="1037"/>
        <source>Extend(\n,\r,\t,\0,\x...)</source>
        <translation type="unfinished">扩展(\n,\r,\t,\0,\x...)</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="200"/>
        <location filename="ui_findwin.h" line="970"/>
        <source>Find Next(F3)</source>
        <translation type="unfinished">查找下一个(F3)</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="203"/>
        <location filename="ui_findwin.h" line="972"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="216"/>
        <location filename="ui_findwin.h" line="974"/>
        <source>Find Prev(F4)</source>
        <translation type="unfinished">查找上一个(F4)</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="219"/>
        <location filename="ui_findwin.h" line="976"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="226"/>
        <location filename="ui_findwin.h" line="978"/>
        <source>Counter(T)</source>
        <translation type="unfinished">计数(T)</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="229"/>
        <location filename="ui_findwin.h" line="980"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="469"/>
        <location filename="ui_findwin.h" line="999"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="1127"/>
        <location filename="ui_findwin.h" line="1040"/>
        <source>Clear All</source>
        <translation type="unfinished">清除全部标记</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="242"/>
        <location filename="ui_findwin.h" line="982"/>
        <source>Find All in Current 
 Document</source>
        <translation type="unfinished">在当前文件中查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="256"/>
        <location filename="ui_findwin.h" line="984"/>
        <source>Find All in All Opened 
 Documents</source>
        <translation type="unfinished">查找所有打开文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="271"/>
        <location filename="findwin.ui" line="540"/>
        <location filename="findwin.ui" line="952"/>
        <location filename="findwin.ui" line="1146"/>
        <location filename="ui_findwin.h" line="987"/>
        <location filename="ui_findwin.h" line="1004"/>
        <location filename="ui_findwin.h" line="1029"/>
        <location filename="ui_findwin.h" line="1041"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="294"/>
        <location filename="findwin.ui" line="488"/>
        <location filename="ui_findwin.h" line="1000"/>
        <location filename="ui_findwin.h" line="1005"/>
        <source>Replace</source>
        <translation type="unfinished">替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="333"/>
        <location filename="findwin.ui" line="636"/>
        <location filename="ui_findwin.h" line="990"/>
        <location filename="ui_findwin.h" line="1009"/>
        <source>Replace with :</source>
        <translation type="unfinished">替换为：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="507"/>
        <location filename="ui_findwin.h" line="1001"/>
        <source>Replace All</source>
        <translation type="unfinished">在当前文件中替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="526"/>
        <location filename="ui_findwin.h" line="1002"/>
        <source>Replace All in All Opened 
 Documents</source>
        <translation type="unfinished">替换所有打开文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="914"/>
        <location filename="ui_findwin.h" line="1027"/>
        <source>Replace In File</source>
        <translation type="unfinished">在目录文件中替换</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="264"/>
        <location filename="findwin.ui" line="933"/>
        <location filename="ui_findwin.h" line="986"/>
        <location filename="ui_findwin.h" line="1028"/>
        <source>Clear Result</source>
        <translation type="unfinished">清空结果</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="975"/>
        <location filename="ui_findwin.h" line="1042"/>
        <source>Mark</source>
        <translation type="unfinished">标记</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="985"/>
        <location filename="ui_findwin.h" line="1031"/>
        <source>Mark What</source>
        <translation type="unfinished">标记目标：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="1101"/>
        <location filename="ui_findwin.h" line="1038"/>
        <source>Mark All</source>
        <translation type="unfinished">全部标记</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="1114"/>
        <location filename="ui_findwin.h" line="1039"/>
        <source>Clear Mark</source>
        <translation type="unfinished">清除</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="563"/>
        <location filename="ui_findwin.h" line="1030"/>
        <source>Dir Find</source>
        <translation type="unfinished">在目录查找</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="575"/>
        <location filename="ui_findwin.h" line="1006"/>
        <source>Dest Dir :</source>
        <translation type="unfinished">目标目录：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="598"/>
        <location filename="ui_findwin.h" line="1007"/>
        <source>Select</source>
        <translation type="unfinished">选择</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="660"/>
        <location filename="ui_findwin.h" line="1010"/>
        <source>File Type</source>
        <translation type="unfinished">文件类型：</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="676"/>
        <location filename="ui_findwin.h" line="1011"/>
        <source>*.c:*.cpp:*.h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="687"/>
        <location filename="ui_findwin.h" line="1012"/>
        <source>Skip Dir Name</source>
        <translation type="unfinished">跳过目录名</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="697"/>
        <location filename="ui_findwin.h" line="1013"/>
        <source>debug:Debug:.vs:.git:.svn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="801"/>
        <location filename="ui_findwin.h" line="1020"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="807"/>
        <location filename="ui_findwin.h" line="1021"/>
        <source>Skip child dirs</source>
        <translation type="unfinished">跳过子目录</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="814"/>
        <location filename="ui_findwin.h" line="1022"/>
        <source>Skip hide file</source>
        <translation type="unfinished">跳过隐藏文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="827"/>
        <location filename="ui_findwin.h" line="1023"/>
        <source>Skip binary file</source>
        <translation type="unfinished">跳过二进制文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="842"/>
        <location filename="ui_findwin.h" line="1024"/>
        <source>Skip Big file exceed</source>
        <translation type="unfinished">跳过超过大小的文件</translation>
    </message>
    <message>
        <location filename="findwin.ui" line="865"/>
        <location filename="ui_findwin.h" line="1025"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="findwin.ui" line="895"/>
        <location filename="ui_findwin.h" line="1026"/>
        <source>Find All</source>
        <translation type="unfinished">全部查找</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="788"/>
        <location filename="findwin.cpp" line="884"/>
        <location filename="findwin.cpp" line="1021"/>
        <location filename="findwin.cpp" line="1112"/>
        <location filename="findwin.cpp" line="1254"/>
        <location filename="findwin.cpp" line="1348"/>
        <location filename="findwin.cpp" line="1418"/>
        <location filename="findwin.cpp" line="1622"/>
        <location filename="findwin.cpp" line="1668"/>
        <location filename="findwin.cpp" line="2211"/>
        <location filename="findwin.cpp" line="2336"/>
        <source>what find is null !</source>
        <translation type="unfinished">查找字段为空</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="818"/>
        <location filename="findwin.cpp" line="1064"/>
        <location filename="findwin.cpp" line="1216"/>
        <location filename="findwin.cpp" line="1462"/>
        <location filename="findwin.cpp" line="1780"/>
        <source>cant&apos;t find text &apos;%1&apos;</source>
        <translation type="unfinished">找不到字段 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="836"/>
        <location filename="findwin.cpp" line="1235"/>
        <source>no more find text &apos;%1&apos;</source>
        <translation type="unfinished">没有更多的字段 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="190"/>
        <location filename="findwin.cpp" line="701"/>
        <location filename="findwin.cpp" line="895"/>
        <location filename="findwin.cpp" line="1032"/>
        <source>The ReadOnly document does not allow this operation.</source>
        <translation type="unfinished">当前只读显示文件不允许该操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1097"/>
        <location filename="findwin.cpp" line="1178"/>
        <source>find finished, total %1 found!</source>
        <translation type="unfinished">查找完成，一共 %1 处发现。</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="952"/>
        <location filename="findwin.cpp" line="1103"/>
        <source>The mode of the current document does not allow this operation.</source>
        <translation type="unfinished">当前模式下的文档不允许该操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1360"/>
        <location filename="findwin.cpp" line="1433"/>
        <location filename="findwin.cpp" line="1637"/>
        <source>The ReadOnly document does not allow replacement.</source>
        <translation type="unfinished">当前只读文档不允许执行替换操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1315"/>
        <location filename="findwin.cpp" line="1322"/>
        <source>no more replace text &apos;%1&apos;</source>
        <translation type="unfinished">没有更多替换文本 &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="679"/>
        <source>find-regex-zero-length-match</source>
        <translation type="unfinished">正则查找零长匹配</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="772"/>
        <source>target info linenum %1 pos is %2 - %3</source>
        <translation type="unfinished">目标在行 %1 位置 %2 - %3</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="920"/>
        <location filename="findwin.cpp" line="948"/>
        <source>count %1 times with &apos;%2&apos;</source>
        <translation type="unfinished">计数 %1 次匹配 &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1373"/>
        <location filename="findwin.cpp" line="1495"/>
        <source>The mode of the current document does not allow replacement.</source>
        <translation type="unfinished">当前模式的文档不允许执行替换操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1422"/>
        <location filename="findwin.cpp" line="1626"/>
        <source>Replace All current Doc</source>
        <translation type="unfinished">在所有打开文件中替换</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1422"/>
        <location filename="findwin.cpp" line="1626"/>
        <source>Are you sure replace all occurrences in current documents?</source>
        <translation type="unfinished">是否确认在当前打开的文档中替换？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1491"/>
        <location filename="findwin.cpp" line="1661"/>
        <source>replace finished, total %1 replaced!</source>
        <translation type="unfinished">替换完成，一共 %1 处替换！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1673"/>
        <source>Replace All Open Doc</source>
        <translation type="unfinished">替换所有打开的文档</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1673"/>
        <source>Are you sure replace all occurrences in all open documents?</source>
        <translation type="unfinished">是否确认在所有打开的文档中替换？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1737"/>
        <source>Replace in Opened Files: %1 occurrences were replaced.</source>
        <translation type="unfinished">在打开的文档中替换：%1 处已经被替换。</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1746"/>
        <location filename="findwin.cpp" line="1847"/>
        <source>what mark is null !</source>
        <translation type="unfinished">标记字段为空！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1790"/>
        <source>cant&apos;t mark text &apos;%1&apos;</source>
        <translation type="unfinished">不能标记文本 ‘%1’</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1833"/>
        <source>mark finished, total %1 found!</source>
        <translation type="unfinished">标记完成，一共 %1 处发现！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1837"/>
        <source>The mode of the current document does not allow mark.</source>
        <translation type="unfinished">当前模式的文档不允许执行标记操作！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="1872"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2032"/>
        <source>load dir file in progress
, please wait ...</source>
        <translation type="unfinished">加载目录文件中，请等待...</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2069"/>
        <location filename="findwin.cpp" line="2077"/>
        <source>skip dir %1</source>
        <translation type="unfinished">跳过目录 %1</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2085"/>
        <source>found %1 dir %2</source>
        <translation type="unfinished">发现 %1 个目录 %2</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2123"/>
        <source>ext type  skip file %1</source>
        <translation type="unfinished">跳过类型文件 %1</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2149"/>
        <source>found in dir canceled ...</source>
        <translation type="unfinished">查找取消...</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2157"/>
        <source>Continue Find ?</source>
        <translation type="unfinished">是否继续查找？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2157"/>
        <source>The search results have been greater than %1 times in %2 files, and more may be slow. Continue to search?</source>
        <translation type="unfinished">查找结果已经有 %1 处在 %2 个文件中，结果太多会比较慢，是否继续查找？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2157"/>
        <source>Yes</source>
        <translation type="unfinished">继续查找</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2157"/>
        <source>Abort</source>
        <translation type="unfinished">终止查找</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2204"/>
        <location filename="findwin.cpp" line="2329"/>
        <source>please select find dest dir !</source>
        <translation type="unfinished">请选择目标文件夹！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2219"/>
        <source>dest dir %1 not exist !</source>
        <translation type="unfinished">目标文件夹 %1 不存在！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2303"/>
        <source>find finished, total %1 found in %2 file!</source>
        <translation type="unfinished">查找完成，一共发现 %1 处在 %2 个文件中！</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2341"/>
        <source>Replace All Dirs</source>
        <translation type="unfinished">目录全部替换</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2341"/>
        <source>Are you sure replace all &quot;%1&quot; to &quot;%2&quot; occurrences in selected dirs ?</source>
        <translation type="unfinished">您确定替换目录文件中所有 &quot;%1&quot; 为 &quot;%2&quot; 吗？</translation>
    </message>
    <message>
        <location filename="findwin.cpp" line="2421"/>
        <source>replace finished, total %1 replace in %2 file!</source>
        <translation type="unfinished">替换完成，一共替换 %1 处在 %2 个文件中！</translation>
    </message>
</context>
<context>
    <name>GoToLineWin</name>
    <message>
        <location filename="gotolinewin.ui" line="20"/>
        <location filename="ui_gotolinewin.h" line="125"/>
        <source>GoToLineWin</source>
        <translation type="unfinished">跳转到行</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="50"/>
        <location filename="ui_gotolinewin.h" line="126"/>
        <source>Line Num</source>
        <translation type="unfinished">行号</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="71"/>
        <location filename="ui_gotolinewin.h" line="127"/>
        <source>Left</source>
        <translation type="unfinished">左边</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="81"/>
        <location filename="ui_gotolinewin.h" line="128"/>
        <source>Right</source>
        <translation type="unfinished">右边</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="108"/>
        <location filename="ui_gotolinewin.h" line="129"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="gotolinewin.ui" line="115"/>
        <location filename="ui_gotolinewin.h" line="130"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>HexCmpRangeWin</name>
    <message>
        <location filename="hexcmprangewin.ui" line="20"/>
        <location filename="ui_hexcmprangewin.h" line="171"/>
        <source>HexCmpRangeWin</source>
        <translation type="unfinished">选择文件对比范围</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="32"/>
        <location filename="ui_hexcmprangewin.h" line="172"/>
        <source>Max Bin File Size is 10M ! Exceeding file size ! 
Select a shorter range for comparison. </source>
        <translation type="unfinished">文件最大对比长度为10M! 当前文件超过最大限制。
请选择一个文件范围来进行对比。</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="40"/>
        <location filename="ui_hexcmprangewin.h" line="174"/>
        <source>Select Range</source>
        <translation type="unfinished">选择对比范围</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="48"/>
        <location filename="ui_hexcmprangewin.h" line="175"/>
        <source>Left Start Pos: </source>
        <translation type="unfinished">左边开始偏移值：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="61"/>
        <location filename="hexcmprangewin.ui" line="108"/>
        <location filename="ui_hexcmprangewin.h" line="176"/>
        <location filename="ui_hexcmprangewin.h" line="180"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="68"/>
        <location filename="ui_hexcmprangewin.h" line="177"/>
        <source>Left Compare Length:</source>
        <translation type="unfinished">左边对比长度：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="81"/>
        <location filename="hexcmprangewin.ui" line="128"/>
        <location filename="ui_hexcmprangewin.h" line="178"/>
        <location filename="ui_hexcmprangewin.h" line="182"/>
        <source>10240</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="95"/>
        <location filename="ui_hexcmprangewin.h" line="179"/>
        <source>Right Start Pos:</source>
        <translation type="unfinished">右边开始偏移值：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="115"/>
        <location filename="ui_hexcmprangewin.h" line="181"/>
        <source>Right Compare Length:</source>
        <translation type="unfinished">右边对比长度：</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="165"/>
        <location filename="ui_hexcmprangewin.h" line="184"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.ui" line="172"/>
        <location filename="ui_hexcmprangewin.h" line="185"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.cpp" line="69"/>
        <source>Error</source>
        <translation type="unfinished">错误</translation>
    </message>
    <message>
        <location filename="hexcmprangewin.cpp" line="69"/>
        <source>StartPos or cmpare lens value error.</source>
        <translation type="unfinished">开始位置或对比长度值错误！</translation>
    </message>
</context>
<context>
    <name>HexFileGoto</name>
    <message>
        <location filename="hexfilegoto.ui" line="20"/>
        <location filename="ui_hexfilegoto.h" line="148"/>
        <source>HexFileGoto</source>
        <translation type="unfinished">二进制跳转</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="45"/>
        <location filename="ui_hexfilegoto.h" line="149"/>
        <source>Addr</source>
        <translation type="unfinished">地址</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="68"/>
        <location filename="ui_hexfilegoto.h" line="150"/>
        <source>Dec Addr</source>
        <translation type="unfinished">10进制地址</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="89"/>
        <location filename="ui_hexfilegoto.h" line="151"/>
        <source>Hex Addr</source>
        <translation type="unfinished">16进制地址</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="142"/>
        <location filename="ui_hexfilegoto.h" line="152"/>
        <source>Go to</source>
        <translation type="unfinished">跳转</translation>
    </message>
    <message>
        <location filename="hexfilegoto.ui" line="149"/>
        <location filename="ui_hexfilegoto.h" line="153"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>HexRuleWin</name>
    <message>
        <location filename="hexrulewin.ui" line="20"/>
        <location filename="ui_hexrulewin.h" line="118"/>
        <source>HexRuleWinRule</source>
        <translation type="unfinished">bin对比规则</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="30"/>
        <location filename="ui_hexrulewin.h" line="119"/>
        <source>Mode</source>
        <translation type="unfinished">模式</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="36"/>
        <location filename="ui_hexrulewin.h" line="120"/>
        <source>Maximum Common String</source>
        <translation type="unfinished">最大公共内容方式</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="46"/>
        <location filename="ui_hexrulewin.h" line="121"/>
        <source>One-to-one Byte Contrast</source>
        <translation type="unfinished">按字节一对一比较</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="56"/>
        <location filename="ui_hexrulewin.h" line="122"/>
        <source>Highlight diff Background</source>
        <translation type="unfinished">高亮不同处背景</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="91"/>
        <location filename="ui_hexrulewin.h" line="123"/>
        <source>OK</source>
        <translation type="unfinished">确认</translation>
    </message>
    <message>
        <location filename="hexrulewin.ui" line="98"/>
        <location filename="ui_hexrulewin.h" line="124"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>LangStyleDefine</name>
    <message>
        <location filename="langstyledefine.cpp" line="105"/>
        <source>Create New Languages</source>
        <translation type="unfinished">新建自定义语言</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="105"/>
        <source>Please Input Languages Name</source>
        <translation type="unfinished">请输入语言名称</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="111"/>
        <source>Name Error</source>
        <translation type="unfinished">名称错误</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="111"/>
        <source>Name can not contains char &apos;.&apos; </source>
        <translation type="unfinished">名称不能包含字符 &apos;.&apos;</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="131"/>
        <source>Ext is empty</source>
        <translation type="unfinished">后缀名为空</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="131"/>
        <source>input ext file tyle. Split with space char</source>
        <translation type="unfinished">请输入关联文件的后缀名，以空格分隔。</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="137"/>
        <source>Keyword is empty</source>
        <translation type="unfinished">关键词为空！</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="137"/>
        <source>input Keyword. Split with space char</source>
        <translation type="unfinished">请输入关键词，以空格分隔。</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="145"/>
        <source>Language name is empty</source>
        <translation type="unfinished">语言名称为空！</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="145"/>
        <source>Select Definition Language Text</source>
        <translation type="unfinished">请选择自定义语言名称</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="176"/>
        <source>Save %1 language finished !</source>
        <translation type="unfinished">保存语言 %1 完成！</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="196"/>
        <source>Delete Language</source>
        <translation type="unfinished">删除语言</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="196"/>
        <source>Are you sure delete user define lanuage %1</source>
        <translation type="unfinished">确定删除自定义语言 %1 吗？</translation>
    </message>
    <message>
        <location filename="langstyledefine.cpp" line="238"/>
        <source>Delete %1 language finished !</source>
        <translation type="unfinished">删除语言 %1 完成！</translation>
    </message>
</context>
<context>
    <name>LangStyleDefineClass</name>
    <message>
        <location filename="langstyledefine.ui" line="14"/>
        <location filename="ui_langstyledefine.h" line="206"/>
        <source>LangStyleDefine</source>
        <translation type="unfinished">自定义语言</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="39"/>
        <location filename="ui_langstyledefine.h" line="207"/>
        <source>Setting</source>
        <translation type="unfinished">设置</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="65"/>
        <location filename="ui_langstyledefine.h" line="208"/>
        <source>Definition Language</source>
        <translation type="unfinished">自定义语言：</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="79"/>
        <location filename="ui_langstyledefine.h" line="209"/>
        <source>Mother Language</source>
        <translation type="unfinished">母版语言：</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="87"/>
        <location filename="ui_langstyledefine.h" line="210"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="92"/>
        <location filename="ui_langstyledefine.h" line="211"/>
        <source>Cpp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="104"/>
        <location filename="ui_langstyledefine.h" line="213"/>
        <source>Delete</source>
        <translation type="unfinished">删除</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="111"/>
        <location filename="ui_langstyledefine.h" line="214"/>
        <source>New Create</source>
        <translation type="unfinished">新建</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="118"/>
        <location filename="ui_langstyledefine.h" line="215"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="125"/>
        <location filename="ui_langstyledefine.h" line="216"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="136"/>
        <location filename="ui_langstyledefine.h" line="217"/>
        <source>Expand File Name:</source>
        <translation type="unfinished">关联文件后缀名：</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="146"/>
        <location filename="ui_langstyledefine.h" line="218"/>
        <source>js cs (split with space）</source>
        <translation type="unfinished">比如 js ts (多个以空格分隔）</translation>
    </message>
    <message>
        <location filename="langstyledefine.ui" line="171"/>
        <location filename="ui_langstyledefine.h" line="219"/>
        <source>Input Key Words</source>
        <translation type="unfinished">输入关键字</translation>
    </message>
</context>
<context>
    <name>LcsLine</name>
    <message>
        <location filename="LcsLine.cpp" line="54"/>
        <source>left Big Step %1, dealed %2 Step, compute %3 step, total %4 steps ...</source>
        <translation type="unfinished">剩余步 %1次，已经处理 %2次， 当前小步对比循环 %3 次，还剩余 %4 次 ...</translation>
    </message>
    <message>
        <location filename="LcsLine.cpp" line="274"/>
        <source>Compare loop %1 times, %2 times left ...</source>
        <translation type="unfinished">对比循环 %1 次，还剩余 %2 次 ...</translation>
    </message>
</context>
<context>
    <name>NetRegister</name>
    <message>
        <source>notice</source>
        <translation type="obsolete">消息</translation>
    </message>
    <message>
        <source>Font Lost</source>
        <translation type="obsolete">字体缺失</translation>
    </message>
    <message>
        <source>Please Install System Font [Courier/SimSun/Times New Roman].The interface font will display exceptions</source>
        <translation type="obsolete">请安装至少一种系统字体[[Courier/SimSun/Times New Roman]。软件界面字体显示可能不满意。</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="225"/>
        <location filename="netregister.cpp" line="229"/>
        <location filename="netregister.cpp" line="242"/>
        <source>The new software has been released, please update it timely!</source>
        <translation type="unfinished">软件新版本已经发布，请及时更新！</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="204"/>
        <location filename="netregister.cpp" line="259"/>
        <source>Software status is normal.</source>
        <translation type="unfinished">软件状态正常</translation>
    </message>
    <message>
        <source>&lt;a href = &quot;www.itdp.cn&quot;&gt;&lt;u&gt;New Version Was Detected&lt;/u&gt;&lt;/a&gt;</source>
        <translation type="obsolete">&lt;a href = &quot;www.itdp.cn&quot;&gt;&lt;u&gt;发现新版本...&lt;/u&gt;&lt;/a&gt;</translation>
    </message>
    <message>
        <source>%1 not exist, please check!</source>
        <translation type="obsolete">文件 %1 不存在，请检查！</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="obsolete">消息</translation>
    </message>
    <message>
        <location filename="netregister.cpp" line="326"/>
        <source>Please contact us. QQ Group:959439826</source>
        <translation type="unfinished">请加入我们QQ群：959439826</translation>
    </message>
    <message>
        <source>Please contact us. QQ:959439826</source>
        <translation type="obsolete">请联系我们QQ群：959439826</translation>
    </message>
    <message>
        <source>Close ?</source>
        <translation type="obsolete">关闭?</translation>
    </message>
    <message>
        <source>already has child window open, close all ?</source>
        <translation type="obsolete">目前还有子窗口处于打开状态，关闭所有窗口吗？</translation>
    </message>
    <message>
        <source>file path not exist, remove recent record!</source>
        <translation type="obsolete">文件路径不存在，删除历史记录！</translation>
    </message>
    <message>
        <source>file [%1] may be not a text file, cmp in hex mode?</source>
        <translation type="obsolete">文件 %1 可能不是文本格式，是否进行二进制格式对比？</translation>
    </message>
</context>
<context>
    <name>OptionsView</name>
    <message>
        <location filename="optionsview.ui" line="14"/>
        <location filename="ui_optionsview.h" line="97"/>
        <source>OptionsView</source>
        <translation type="unfinished">设置</translation>
    </message>
    <message>
        <location filename="optionsview.ui" line="24"/>
        <location filename="ui_optionsview.h" line="98"/>
        <source>Options</source>
        <translation type="unfinished">选项</translation>
    </message>
    <message>
        <location filename="optionsview.ui" line="63"/>
        <location filename="ui_optionsview.h" line="99"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>file correlation</source>
        <translation type="obsolete">文件关联</translation>
    </message>
    <message>
        <location filename="optionsview.cpp" line="19"/>
        <source>File Correlation</source>
        <translation type="unfinished">文件关联</translation>
    </message>
    <message>
        <location filename="optionsview.cpp" line="27"/>
        <source>Compare File Types</source>
        <translation type="unfinished">对比文件类型</translation>
    </message>
    <message>
        <location filename="optionsview.cpp" line="33"/>
        <source>Text And Fonts</source>
        <translation type="unfinished">文本和字体</translation>
    </message>
</context>
<context>
    <name>ProgressWin</name>
    <message>
        <location filename="progresswin.ui" line="17"/>
        <location filename="ui_progresswin.h" line="105"/>
        <source>work progress</source>
        <translation type="unfinished">工作进度</translation>
    </message>
    <message>
        <location filename="progresswin.ui" line="51"/>
        <location filename="ui_progresswin.h" line="106"/>
        <source>current progress</source>
        <translation type="unfinished">当前进度</translation>
    </message>
    <message>
        <location filename="progresswin.ui" line="85"/>
        <location filename="ui_progresswin.h" line="107"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <location filename="progresswin.cpp" line="67"/>
        <source>Notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="progresswin.cpp" line="67"/>
        <source>Are you sure to cancel?</source>
        <translation type="unfinished">您确定取消吗？</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="123"/>
        <source>Text Mode</source>
        <translation type="unfinished">文本模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="126"/>
        <source>Hex ReadOnly Mode</source>
        <translation type="unfinished">二进制只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="129"/>
        <source>Bit Text ReadOnly Mode</source>
        <translation type="unfinished">大文本只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="132"/>
        <source>Text ReadOnly Mode</source>
        <translation type="unfinished">文本只读模式</translation>
    </message>
    <message>
        <location filename="cceditor/ccnotepad.cpp" line="135"/>
        <source>File Mode</source>
        <translation type="unfinished">文件模式</translation>
    </message>
</context>
<context>
    <name>QsciDisplayWindow</name>
    <message>
        <location filename="qscidisplaywindow.cpp" line="489"/>
        <source>Find Text</source>
        <translation type="unfinished">查找文本</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="490"/>
        <source>Show File in Explorer</source>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="491"/>
        <source>Save As ...</source>
        <translation type="unfinished">另存为</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="557"/>
        <location filename="qscidisplaywindow.cpp" line="577"/>
        <source>Not Find</source>
        <translation type="unfinished">没有找到</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="557"/>
        <source>Not Find Next!</source>
        <translation type="unfinished">找不到下一个！</translation>
    </message>
    <message>
        <location filename="qscidisplaywindow.cpp" line="577"/>
        <source>Not Find Prev!</source>
        <translation type="unfinished">找不到前一个！</translation>
    </message>
</context>
<context>
    <name>QsciLexerText</name>
    <message>
        <source>Chinese And Others</source>
        <translation type="unfinished">中文字符及其它</translation>
    </message>
    <message>
        <source>Ascii</source>
        <translation type="unfinished">英文字符</translation>
    </message>
    <message>
        <source>Keyword</source>
        <translation type="unfinished">关键词</translation>
    </message>
</context>
<context>
    <name>QsciScintilla</name>
    <message>
        <source>&amp;Cut</source>
        <translation type="unfinished">剪切</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="unfinished">复制</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="unfinished">粘贴</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished">删除</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation type="unfinished">全选</translation>
    </message>
    <message>
        <source>Show File in Explorer</source>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
</context>
<context>
    <name>QtLangSet</name>
    <message>
        <location filename="qtlangset.cpp" line="745"/>
        <source>Save Change</source>
        <translation type="unfinished">保存修改</translation>
    </message>
    <message>
        <source>The current style configuration has been modified. Do you want to save it?</source>
        <translation type="obsolete">当前语言的格式风格已经被修改，是否保存？</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="667"/>
        <source>AllGlobal</source>
        <translation type="unfinished">全局格式统一设置</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="745"/>
        <source>%1 style configuration has been modified. Do you want to save it?</source>
        <translation type="unfinished">%1 类型的显示风格已经被修改，是否保存？</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="936"/>
        <source>Read %1 language user define  format error.</source>
        <translation type="unfinished">读取 %1 语言用户自定义格式失败！</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="1064"/>
        <source>Style Foreground Color</source>
        <translatorcomment>风格背景色</translatorcomment>
        <translation type="unfinished">风格前景色</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="1141"/>
        <source>Style Background Color</source>
        <translation type="unfinished">风格背景色</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="1216"/>
        <source>Reset Style</source>
        <translation type="unfinished">重置风格</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="1216"/>
        <source>Are you sure to reset language %1 sytle</source>
        <translation type="unfinished">您确定重置语言 %1 的风格吗？</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="1255"/>
        <source>Reset All Style</source>
        <translation type="unfinished">重置所有风格</translation>
    </message>
    <message>
        <location filename="qtlangset.cpp" line="1255"/>
        <source>Are you sure to reset All language sytle</source>
        <translation type="unfinished">您确定重置所有语言风格吗？</translation>
    </message>
</context>
<context>
    <name>QtLangSetClass</name>
    <message>
        <location filename="qtlangset.ui" line="14"/>
        <location filename="ui_qtlangset.h" line="503"/>
        <source>QtLangSet</source>
        <translation type="unfinished">编程语言风格设置</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="27"/>
        <location filename="ui_qtlangset.h" line="504"/>
        <source>Language</source>
        <translation type="unfinished">语言</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="50"/>
        <location filename="ui_qtlangset.h" line="505"/>
        <source>User Define Language</source>
        <translation type="unfinished">自定义语言</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="77"/>
        <location filename="ui_qtlangset.h" line="506"/>
        <source>Style</source>
        <translation type="unfinished">语法风格项</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="104"/>
        <location filename="ui_qtlangset.h" line="507"/>
        <source>Color</source>
        <translation type="unfinished">颜色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="112"/>
        <location filename="ui_qtlangset.h" line="508"/>
        <source>Foreground:</source>
        <translation type="unfinished">前景色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="131"/>
        <location filename="ui_qtlangset.h" line="509"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="138"/>
        <location filename="qtlangset.ui" line="195"/>
        <location filename="ui_qtlangset.h" line="510"/>
        <location filename="ui_qtlangset.h" line="514"/>
        <source>Select</source>
        <translation type="unfinished">选择</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="145"/>
        <location filename="qtlangset.ui" line="287"/>
        <location filename="ui_qtlangset.h" line="511"/>
        <location filename="ui_qtlangset.h" line="521"/>
        <source>All Style</source>
        <translation type="unfinished">修改所有风格</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="169"/>
        <location filename="ui_qtlangset.h" line="512"/>
        <source>background:</source>
        <translation type="unfinished">背景色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="188"/>
        <location filename="ui_qtlangset.h" line="513"/>
        <source>Same As Theme</source>
        <translation type="unfinished">与主题保存一致</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="220"/>
        <location filename="ui_qtlangset.h" line="515"/>
        <source>Font</source>
        <translation type="unfinished">字体</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="271"/>
        <location filename="ui_qtlangset.h" line="520"/>
        <source>Font Size:</source>
        <translation type="unfinished">字体大小</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="244"/>
        <location filename="ui_qtlangset.h" line="517"/>
        <source>Bold</source>
        <translation type="unfinished">粗体</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="251"/>
        <location filename="ui_qtlangset.h" line="518"/>
        <source>Italic</source>
        <translation type="unfinished">斜体</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="258"/>
        <location filename="ui_qtlangset.h" line="519"/>
        <source>Underline</source>
        <translation type="unfinished">下划线</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="228"/>
        <location filename="ui_qtlangset.h" line="516"/>
        <source>Name:</source>
        <translation type="unfinished">名称：</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="308"/>
        <location filename="ui_qtlangset.h" line="522"/>
        <source>Keyword And Mother </source>
        <translation type="unfinished">关键词和母版</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="323"/>
        <location filename="ui_qtlangset.h" line="523"/>
        <source>Mother Language:</source>
        <translation type="unfinished">母版语言：</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="334"/>
        <location filename="ui_qtlangset.h" line="524"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="339"/>
        <location filename="ui_qtlangset.h" line="525"/>
        <source>Cpp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="351"/>
        <location filename="ui_qtlangset.h" line="527"/>
        <source>Ext File Type:</source>
        <translation type="unfinished">关联文件后缀名：</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="374"/>
        <location filename="ui_qtlangset.h" line="528"/>
        <source>Global Style Set</source>
        <translation type="unfinished">全局风格设置</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="380"/>
        <location filename="ui_qtlangset.h" line="529"/>
        <source>Use Global Foreground Color</source>
        <translation type="unfinished">使用全局前景色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="390"/>
        <location filename="ui_qtlangset.h" line="530"/>
        <source>Use Global Background Color</source>
        <translation type="unfinished">使用全局背景色</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="397"/>
        <location filename="ui_qtlangset.h" line="531"/>
        <source>Use Global Font</source>
        <translation type="unfinished">使用全局字体</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="404"/>
        <location filename="ui_qtlangset.h" line="532"/>
        <source>Use Global FontSize</source>
        <translation type="unfinished">使用全局字体大小</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="411"/>
        <location filename="ui_qtlangset.h" line="533"/>
        <source>Use Global Bold Font</source>
        <translation type="unfinished">使用全局粗体样式</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="418"/>
        <location filename="ui_qtlangset.h" line="534"/>
        <source>Use Global Italic Font</source>
        <translation type="unfinished">使用全局斜体样式</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="425"/>
        <location filename="ui_qtlangset.h" line="535"/>
        <source>Use Global Underline Font</source>
        <translation type="unfinished">使用全局下划线样式</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="441"/>
        <location filename="ui_qtlangset.h" line="536"/>
        <source>reset</source>
        <translation type="unfinished">重置</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="448"/>
        <location filename="ui_qtlangset.h" line="537"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="qtlangset.ui" line="455"/>
        <location filename="ui_qtlangset.h" line="538"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>RcTreeWidget</name>
    <message>
        <location filename="RcTreeWidget.cpp" line="47"/>
        <source>Show File in Explorer</source>
        <oldsource>Show File in Explorer...</oldsource>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
</context>
<context>
    <name>ReNameWin</name>
    <message>
        <location filename="renamewin.ui" line="20"/>
        <location filename="ui_renamewin.h" line="273"/>
        <source>ReNameBatchWin</source>
        <translation type="unfinished">批量重命名</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="32"/>
        <location filename="ui_renamewin.h" line="274"/>
        <source>Select Dir</source>
        <translation type="unfinished">选择目录</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="42"/>
        <location filename="ui_renamewin.h" line="275"/>
        <source>Browse</source>
        <translation type="unfinished">浏览</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="55"/>
        <location filename="ui_renamewin.h" line="282"/>
        <source>AddDelNameString</source>
        <translation type="unfinished">增加删除</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="61"/>
        <location filename="ui_renamewin.h" line="276"/>
        <source>Add Prefix</source>
        <translation type="unfinished">增加前缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="75"/>
        <location filename="ui_renamewin.h" line="277"/>
        <source>Del Prefix</source>
        <translation type="unfinished">删除前缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="89"/>
        <location filename="ui_renamewin.h" line="278"/>
        <source>Add Suffix</source>
        <translation type="unfinished">增加后缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="103"/>
        <location filename="ui_renamewin.h" line="279"/>
        <source>Del Suffix</source>
        <translation type="unfinished">删除后缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="117"/>
        <location filename="ui_renamewin.h" line="280"/>
        <source>Lower FileName</source>
        <translation type="unfinished">小写文件名</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="124"/>
        <location filename="ui_renamewin.h" line="281"/>
        <source>Upper FileName</source>
        <translation type="unfinished">大写文件名</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="132"/>
        <location filename="ui_renamewin.h" line="288"/>
        <source>ChangeExt</source>
        <translation type="unfinished">改变后缀</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="140"/>
        <location filename="ui_renamewin.h" line="283"/>
        <source>Modify Ext To</source>
        <translation type="unfinished">修改文件后缀为</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="167"/>
        <location filename="ui_renamewin.h" line="284"/>
        <source>Deal Ext Type</source>
        <translation type="unfinished">处理的后缀类型</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="181"/>
        <location filename="ui_renamewin.h" line="285"/>
        <source>All File Ext Type</source>
        <translation type="unfinished">所有支持的类型</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="189"/>
        <location filename="ui_renamewin.h" line="287"/>
        <source>defined</source>
        <translation type="unfinished">自定义</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="228"/>
        <location filename="ui_renamewin.h" line="289"/>
        <source>Deal Child Dir</source>
        <translation type="unfinished">处理子目录</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="250"/>
        <location filename="ui_renamewin.h" line="290"/>
        <source>Start</source>
        <translation type="unfinished">开始</translation>
    </message>
    <message>
        <location filename="renamewin.ui" line="257"/>
        <location filename="ui_renamewin.h" line="291"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="32"/>
        <source>Open Directory</source>
        <translation type="unfinished">打开目录</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="66"/>
        <source>input file ext()</source>
        <translation type="unfinished">输入文件后缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="66"/>
        <source>ext (Start With .)</source>
        <translation type="unfinished">后缀（以.开头）</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="97"/>
        <location filename="renamewin.cpp" line="107"/>
        <location filename="renamewin.cpp" line="119"/>
        <location filename="renamewin.cpp" line="131"/>
        <location filename="renamewin.cpp" line="143"/>
        <location filename="renamewin.cpp" line="166"/>
        <location filename="renamewin.cpp" line="306"/>
        <location filename="renamewin.cpp" line="322"/>
        <location filename="renamewin.cpp" line="458"/>
        <source>Notice</source>
        <translation type="unfinished">消息</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="97"/>
        <source>Please Select Dir</source>
        <translation type="unfinished">请选择目录</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="107"/>
        <source>Please Input Add File Prefix</source>
        <translation type="unfinished">请选择增加的文件前缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="119"/>
        <source>Please Input Del File Prefix</source>
        <translation type="unfinished">请选择删除的文件前缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="131"/>
        <source>Please Input Add File Suffix</source>
        <translation type="unfinished">请选择增加的文件后缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="143"/>
        <source>Please Input Del File Suffix</source>
        <translation type="unfinished">请选择删除的文件后缀</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="166"/>
        <source>Please Select One Operator</source>
        <translation type="unfinished">请选择一个操作</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="229"/>
        <location filename="renamewin.cpp" line="372"/>
        <source>rename file in progress, please wait ...</source>
        <translation type="unfinished">正在重命名文件中，请等待...</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="293"/>
        <location filename="renamewin.cpp" line="444"/>
        <source>failed %1 file path %2, please check</source>
        <translation type="unfinished">失败 %1 个，文件路径 %2 请检查</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="306"/>
        <location filename="renamewin.cpp" line="458"/>
        <source>Deal Finished, totol %1 files, failed %2 files</source>
        <translation type="unfinished">处理完成，一共 %1 个文件，失败 %2 个文件。</translation>
    </message>
    <message>
        <location filename="renamewin.cpp" line="322"/>
        <source>Please Select Dir Or Dest Ext</source>
        <translation type="unfinished">请选择目录或目标文件后缀</translation>
    </message>
</context>
<context>
    <name>RealCompareClass</name>
    <message>
        <source>MainPage</source>
        <translation type="obsolete">主页</translation>
    </message>
    <message>
        <source>dir compare</source>
        <translation type="obsolete">目录对比</translation>
    </message>
    <message>
        <source>file compare</source>
        <translation type="obsolete">文件对比</translation>
    </message>
    <message>
        <source>encode convert</source>
        <translation type="obsolete">编码转换</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>donate</source>
        <translation type="obsolete">捐赠软件</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="obsolete">语言</translation>
    </message>
    <message>
        <source>bin compare</source>
        <translation type="obsolete">二进制对比</translation>
    </message>
    <message>
        <source>Recently</source>
        <translation type="obsolete">最近对比</translation>
    </message>
    <message>
        <source>Dir ...</source>
        <translation type="obsolete">目录...</translation>
    </message>
    <message>
        <source>File ...</source>
        <translation type="obsolete">文件...</translation>
    </message>
    <message>
        <source>dir</source>
        <translation type="obsolete">目录</translation>
    </message>
    <message>
        <source>file</source>
        <translation type="obsolete">文件</translation>
    </message>
    <message>
        <source>rule</source>
        <translation type="obsolete">规则</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">英文</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="obsolete">中文</translation>
    </message>
    <message>
        <source>main</source>
        <translation type="obsolete">主页</translation>
    </message>
</context>
<context>
    <name>ScintillaEditView</name>
    <message>
        <location filename="scintillaeditview.cpp" line="1227"/>
        <source>Show File in Explorer</source>
        <translation type="unfinished">定位到文件目录</translation>
    </message>
    <message>
        <location filename="scintillaeditview.cpp" line="1231"/>
        <source>mark with color</source>
        <translation type="unfinished">使用颜色标记</translation>
    </message>
    <message>
        <location filename="scintillaeditview.cpp" line="1242"/>
        <source>Color %1</source>
        <translation type="unfinished">颜色 %1</translation>
    </message>
    <message>
        <location filename="scintillaeditview.cpp" line="1267"/>
        <source>Clear Select</source>
        <translation type="unfinished">清除选择标记</translation>
    </message>
    <message>
        <location filename="scintillaeditview.cpp" line="1268"/>
        <source>Clear All</source>
        <translation type="unfinished">清除全部标记</translation>
    </message>
</context>
<context>
    <name>StatusWidget</name>
    <message>
        <location filename="statuswidget.ui" line="32"/>
        <location filename="ui_statuswidget.h" line="51"/>
        <source>notice msg</source>
        <translation type="unfinished">通知消息</translation>
    </message>
</context>
<context>
    <name>TextEditSetWin</name>
    <message>
        <location filename="texteditsetwin.ui" line="14"/>
        <location filename="ui_texteditsetwin.h" line="179"/>
        <source>TextEditSetWin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="22"/>
        <location filename="ui_texteditsetwin.h" line="180"/>
        <source>Tab Setting</source>
        <translation type="unfinished">Tab 设置</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="30"/>
        <location filename="ui_texteditsetwin.h" line="181"/>
        <source>Set Tab Length</source>
        <translation type="unfinished">Tab 字符长度</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="49"/>
        <location filename="ui_texteditsetwin.h" line="182"/>
        <source>Space Replacement Tab</source>
        <translation type="unfinished">使用空格替换tab</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="62"/>
        <location filename="ui_texteditsetwin.h" line="183"/>
        <source>Big Text Size</source>
        <translation type="unfinished">大文本文件大小</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="68"/>
        <location filename="ui_texteditsetwin.h" line="184"/>
        <source>Beyond this size, it can only be read-only and displayed in blocks</source>
        <translation type="unfinished">文本文件超过该值时，只能以只读的方式进行分块加载显示。</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="77"/>
        <location filename="ui_texteditsetwin.h" line="185"/>
        <source>Exceed(MB)</source>
        <translation type="unfinished">超过(MB)</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="97"/>
        <location filename="ui_texteditsetwin.h" line="186"/>
        <source>(50-300MB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="139"/>
        <location filename="ui_texteditsetwin.h" line="187"/>
        <source>Remember files opened on close</source>
        <translation type="unfinished">记住最后打开的文件</translation>
    </message>
    <message>
        <location filename="texteditsetwin.ui" line="146"/>
        <location filename="ui_texteditsetwin.h" line="188"/>
        <source>Txt File Font Set</source>
        <translation type="unfinished">txt 文件字体设置</translation>
    </message>
    <message>
        <source>Txt Font Set（Only Text File)</source>
        <oldsource>Txt Font Set</oldsource>
        <translation type="obsolete">文本默认字体设置（只对TXT文本有效）</translation>
    </message>
    <message>
        <source>Programming Language Default Font</source>
        <translation type="obsolete">编程语言默认字体</translation>
    </message>
    <message>
        <source>Txt Font Set(Only Windows)</source>
        <translation type="obsolete">文本默认字体设置</translation>
    </message>
    <message>
        <source>Default Font</source>
        <translation type="obsolete">默认字体</translation>
    </message>
    <message>
        <source>Select Font</source>
        <translation type="obsolete">选择字体</translation>
    </message>
    <message>
        <location filename="texteditsetwin.cpp" line="122"/>
        <location filename="texteditsetwin.cpp" line="155"/>
        <source>User define Txt Font</source>
        <translation type="unfinished">用户自定义文本字体</translation>
    </message>
</context>
<context>
    <name>TextFind</name>
    <message>
        <location filename="textfind.ui" line="14"/>
        <location filename="ui_textfind.h" line="108"/>
        <source>TextFind</source>
        <translation type="unfinished">查找文本</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="24"/>
        <location filename="ui_textfind.h" line="109"/>
        <source>Find Options</source>
        <translation type="unfinished">查找选项</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="32"/>
        <location filename="ui_textfind.h" line="110"/>
        <source>Find Text</source>
        <translation type="unfinished">查找文本</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="49"/>
        <location filename="ui_textfind.h" line="111"/>
        <source>Find Prev</source>
        <translation type="unfinished">查找前一个</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="56"/>
        <location filename="ui_textfind.h" line="112"/>
        <source>Find Next</source>
        <translation type="unfinished">查找下一个</translation>
    </message>
    <message>
        <location filename="textfind.ui" line="63"/>
        <location filename="ui_textfind.h" line="113"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
</context>
<context>
    <name>UserRegister</name>
    <message>
        <location filename="userregister.cpp" line="21"/>
        <source>Free Trial</source>
        <translation type="unfinished">免费永久试用版本（捐赠可获取注册码）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="25"/>
        <location filename="userregister.cpp" line="58"/>
        <source>Registered Version</source>
        <translation type="unfinished">注册过的正版软件！（恭喜）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="29"/>
        <source>License Expired</source>
        <translation type="unfinished">许可证过期（可捐赠获取）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="33"/>
        <source>License Error</source>
        <translation type="unfinished">错误的注册码（可重新输入）</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="73"/>
        <location filename="userregister.cpp" line="79"/>
        <location filename="userregister.cpp" line="86"/>
        <location filename="userregister.cpp" line="109"/>
        <location filename="userregister.cpp" line="113"/>
        <location filename="userregister.cpp" line="119"/>
        <source>Licence Key</source>
        <translation type="unfinished">许可证</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="73"/>
        <source>It is already a registered version.</source>
        <translation type="unfinished">当前已经是注册版！</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="79"/>
        <source>Please scanning the donation, Write your email address in the message area.
You will get the registration code!</source>
        <translation type="unfinished">请微信扫描捐赠，在留言处留下您的邮件地址，注册码将通过邮件发送给您。</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="86"/>
        <source>Please enter the correct registration code!</source>
        <translation type="unfinished">请输入正确的注册码！</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="109"/>
        <source>Congratulations on your successful registration.</source>
        <translation type="unfinished">恭喜注册成功！</translation>
    </message>
    <message>
        <location filename="userregister.cpp" line="113"/>
        <location filename="userregister.cpp" line="119"/>
        <source>Registration failed. Please try again later.</source>
        <translation type="unfinished">注册失败，请稍后再试！</translation>
    </message>
    <message>
        <source>Please scanning the donation, get and enter the correct code !</source>
        <translation type="obsolete">请微信扫码赞赏后，输入转账单号后11位到许可序列码中。</translation>
    </message>
    <message>
        <source>Please check code length , Only the last 11 digits are required !</source>
        <translation type="obsolete">请检查许可序列长度，只需要后11位即可。</translation>
    </message>
    <message>
        <source>Processing succeeded. We will process your registration code in the background. It may take 1-3 days.</source>
        <translation type="obsolete">处理成功。我们将在1-3天内处理您的许可注册码。</translation>
    </message>
</context>
<context>
    <name>UserRegisterClass</name>
    <message>
        <location filename="userregister.ui" line="20"/>
        <location filename="ui_userregister.h" line="191"/>
        <source>UserRegister</source>
        <translation type="unfinished">捐赠获取注册码</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="115"/>
        <location filename="userregister.ui" line="121"/>
        <location filename="ui_userregister.h" line="194"/>
        <location filename="ui_userregister.h" line="195"/>
        <source>Status</source>
        <translation type="unfinished">软件状态</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="138"/>
        <location filename="ui_userregister.h" line="196"/>
        <source>Machine Id</source>
        <translation type="unfinished">机器码</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="155"/>
        <location filename="ui_userregister.h" line="197"/>
        <source>Licence Key</source>
        <translation type="unfinished">序列号</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="165"/>
        <location filename="ui_userregister.h" line="198"/>
        <source>22874567148</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="userregister.ui" line="177"/>
        <location filename="ui_userregister.h" line="199"/>
        <source>Register</source>
        <translation type="unfinished">注册</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="184"/>
        <location filename="ui_userregister.h" line="200"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="userregister.ui" line="193"/>
        <location filename="ui_userregister.h" line="201"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wechat scans the appreciation code on the left.&lt;/p&gt;&lt;p&gt;Leave your contact email in the donation message.&lt;/p&gt;&lt;p&gt;We will send you the registration code later.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">请微信扫码赞赏软件。
在留言处留下您的邮件地址，注册码将通过邮件发送给您。
在序列号处输入您得到的注册码，点击注册获取注册版软件
您可以免费使用该软件，您也可以捐赠我们获取注册版。</translation>
    </message>
</context>
<context>
    <name>closeDlg</name>
    <message>
        <location filename="closeDlg.ui" line="26"/>
        <location filename="ui_closeDlg.h" line="150"/>
        <source>closeDlg</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="56"/>
        <location filename="ui_closeDlg.h" line="151"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished">是否保存修改？</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="63"/>
        <location filename="ui_closeDlg.h" line="152"/>
        <source>save left document ?</source>
        <translation type="unfinished">保存左边文档？</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="73"/>
        <location filename="ui_closeDlg.h" line="153"/>
        <source>save right document ?</source>
        <translation type="unfinished">保存右边文档？</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="143"/>
        <location filename="ui_closeDlg.h" line="154"/>
        <source>save selected</source>
        <translation type="unfinished">保存所选文档</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="162"/>
        <location filename="ui_closeDlg.h" line="155"/>
        <source>discard</source>
        <translation type="unfinished">放弃修改</translation>
    </message>
    <message>
        <location filename="closeDlg.ui" line="181"/>
        <location filename="ui_closeDlg.h" line="156"/>
        <source>cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
</context>
<context>
    <name>donate</name>
    <message>
        <source>donate</source>
        <translation type="obsolete">捐赠作者</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Donation Software Development By WeChat &lt;/p&gt;&lt;p&gt;Busy living, no time to improve software&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">现实生活中的一地鸡毛
让我无法投入更多时间完善免费软件
请通过微信扫码捐赠作者</translation>
    </message>
    <message>
        <source>Donation Software Development By WeChat </source>
        <translation type="obsolete">通过微信捐赠CC对比软件发展
感谢支持国产软件！</translation>
    </message>
</context>
</TS>
